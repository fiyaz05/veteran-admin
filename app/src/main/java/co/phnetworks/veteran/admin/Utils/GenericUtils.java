package co.phnetworks.veteran.admin.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import co.phnetworks.veteran.admin.App;
import co.phnetworks.veteran.admin.R;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;


public class GenericUtils {

    private static String[][] COUNTRIES = getCountriesFromJSON(App.getApp());

    /**
     * Posts a HTTP POST request and returns the response as a string
     *
     * @param url        URL
     * @param parameters Hashmap of parameters
     * @return HTTP response body
     * @throws IOException
     */
//    public static String postHttpRequest(String url, HashMap<String, String> parameters) throws HttpRequest.HttpRequestException, IOException {
//
//        HttpRequest httpRequest = HttpRequest
//                .post(url)
//                .acceptJson()
//                .form(parameters);
//
//        try {
//            if (httpRequest.ok()) {
//                return httpRequest.body();
//            }
//        } catch (Exception ignored) {
//        }
//
//        throw new IOException();
//    }
    public static String postHttpRequest(String url, HashMap<String, String> parameters) throws IOException {


        FormBody.Builder form = new FormBody.Builder();

        for (String key : parameters.keySet()) {
            form.add(key, parameters.get(key));
        }

        Request request = new Request.Builder()
                .url(url)
                .post(form.build())
                .build();
        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {
            return response.body().string();
        }

        throw new IOException();

    }

    /**
     * Shows simple dialog message. Use instead of toast
     *
     * @param resourceId resource id
     * @param context    Context
     */
    public static void showSimpleDialogMessage(int resourceId, Context context) {
        showSimpleDialogMessage(resourceId, null, context);
    }

    /**
     * Shows simple dialog message. Use instead of toast
     *
     * @param message String
     * @param context Context
     */
    public static void showSimpleDialogMessage(String message, Context context) {
        showSimpleDialogMessage(message, null, context);
    }

    /**
     * Shows simple dialog message. Use instead of toast
     *
     * @param resourceId resource id
     * @param context    Context
     */
    public static void showSimpleDialogMessage(int resourceId, final Runnable runnable, Context context) {

        Activity activity = (Activity) context;
        if (activity.isFinishing()) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(resourceId)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    /**
     * Shows simple dialog message. Use instead of toast
     *
     * @param message String
     * @param context Context
     */
    public static void showSimpleDialogMessage(String message, final Runnable runnable, Context context) {

        Activity activity = (Activity) context;
        if (activity.isFinishing()) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (runnable != null) {
                            runnable.run();
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (in upper case)
     *
     * @param context Context
     * @return String
     */
    public static String getCountryCode(Context context) {

        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toUpperCase();
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toUpperCase();
                }
            }

//	        return context.getResources().getConfiguration().locale.getCountry().toUpperCase();
            // Default to India for now
            return "IN";
        } catch (Exception ignored) {
        }
        return null;
    }

    /**
     * Returns countries details from countries.json in the assets
     *
     * @param context Context
     * @return 2-D string of countries
     */
    public static String[][] getCountriesFromJSON(Context context) {

        if (COUNTRIES != null) {
            return COUNTRIES;
        }

        if (context == null) {
            return null;
        }

        String json;
        try {

            InputStream is = context.getAssets().open("countries.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

            COUNTRIES = new GsonBuilder().create().fromJson(json, String[][].class);

        } catch (IOException ex) {
            return null;
        }

        return COUNTRIES;
    }

    /**
     * Check if Google Play Services is installed
     *
     * @param context Context
     * @return boolean
     */
    public static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        return resultCode == ConnectionResult.SUCCESS;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Checks if Internet is available
     *
     * @param context Context
     * @return boolean
     */
    public static boolean isInternetAvailable(Context context) {

        if (context == null) {
            return false;
        }

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Checks if connected to the Internet via WiFi
     *
     * @param context Context
     * @return Boolean
     */
    public static boolean isWifiAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ((null != ni) && (ni.isConnected()) && (ni.getType() == ConnectivityManager.TYPE_WIFI));
    }

    /**
     * Gets height in cm
     *
     * @param feet   feet
     * @param inches inches
     * @return float
     */
    public static float getHeightInCm(int feet, int inches) {

        float cm = 0;
        cm += (feet * 30.48);
        cm += (inches * 2.54);

        return cm;
    }

    /**
     * Gets weight in kg
     *
     * @param pounds pounds
     * @return float
     */
    public static float getWeightInKg(float pounds) {
        return (float) (pounds * 0.453592);
    }

    /**
     * Gets feet and inches from cm
     *
     * @param cm float
     * @return array of integers
     */
    public static int[] getFeetInches(float cm) {

        int[] result = new int[2];
        result[0] = (int) (cm / 30.48);
        result[1] = (int) (Math.round((cm / 2.54)) - (result[0] * 12));

        return result;
    }

    /**
     * Compares floats
     *
     * @param number1 float
     * @param number2 float
     * @return boolean
     */
    public static boolean compareFloat(float number1, float number2) {

        float sEpsilon = 0.00000001f;
        return Math.abs(number1 - number2) < sEpsilon;

    }

    /**
     * Creates a temporary file where the camera intent will write the captured
     * image
     *
     * @return File
     * @throws IOException
     */
    public static File createTemporaryImageFile() throws IOException {

        String imageFileName = "image-" +
                System.currentTimeMillis() + ".jpg";
        File storageDir = App.getApp().getExternalCacheDir();

        File image = new File(storageDir, imageFileName);
        if (image.createNewFile()) {
            return image;
        }

        return null;
    }

    /**
     * Creates an Image file in the directory given
     *
     * @param directory Path to directory
     * @return File
     * @throws IOException
     */
    public static File createImageFile(String directory) throws IOException {

        String imageFileName = "image-" +
                System.currentTimeMillis() + ".jpg";

        File image = new File(directory, imageFileName);
        if (image.createNewFile()) {
            return image;
        }

        return null;

    }

    /**
     * Downloads a file from the URL and stores with given file name
     *
     * @param urlString URL link
     * @param fileName  The file name to store the file as
     * @return Whether the download was successful or not
     */
    public static boolean downloadFile(String urlString, String fileName) {

        if (TextUtils.isEmpty(urlString) || TextUtils.isEmpty(fileName)) {
            return false;
        }

        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return false;
            }

            // download the file
            input = connection.getInputStream();
            output = App.getApp().openFileOutput(fileName, Context.MODE_PRIVATE);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            return true;

        } catch (Exception e) {
            return false;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    /**
     * Copies the file
     *
     * @param uri      URI
     * @param fileName File name
     * @param context  Context
     * @throws IOException
     */
    public static boolean copyFileAs(Uri uri, String fileName, Context context) throws IOException {

        InputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            // read this file into InputStream
            inputStream = context.getContentResolver().openInputStream(uri);

            if (inputStream == null) {
                return false;
            }

            // write the inputStream to a FileOutputStream
            outputStream = App.getApp().openFileOutput(fileName, Context.MODE_PRIVATE);

            int read;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }

            return true;

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        return false;
    }

    /**
     * Checks if file exists
     *
     * @param fileName file name
     * @return boolean
     */
    public static boolean doesFileExist(String fileName) {
        File file = new File(App.getApp().getFilesDir().getAbsolutePath() + "/" + fileName);
        return file.exists();
    }

    /**
     * check directory whether exist, if not then make one;
     *
     * @param path absolute path of directory
     * @return Check if the operation succeeded
     */
    public static boolean checkDir(String path) {
        boolean result = true;
        File f = new File(path);
        if (!f.exists()) {
            result = f.mkdirs();
        } else if (f.isFile()) {
            f.delete();
            result = f.mkdirs();
        }
        return result;
    }

    public static Bitmap loadBitmap(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap b = null;
        try {
            options.inSampleSize = 1;
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);
            if (options.mCancel || options.outWidth == -1
                    || options.outHeight == -1) {
                return null;
            }
            options.inSampleSize = 2;//BitmapUtils.calculateInSampleSize(options, 595 - 200, 942 - 200);;
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            b = BitmapFactory.decodeFile(path, options);
            if (b == null) {
                return null;
            }

            int orientation = getOrientation(path);
            if (orientation != 1) {
                Matrix m = new Matrix();
                m.postRotate(getRotation(orientation));
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, false);
            }

        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return b;
    }

    /**
     * get rotation degrees
     *
     * @param orientation values in {1, 3, 6, 8}
     * @return values in {0, 90, 180, 270}
     */
    private static int getRotation(int orientation) {
        switch (orientation) {
            case 1:
                return 0;
            case 8:
                return 270;
            case 3:
                return 180;
            case 6:
                return 90;
            default:
                return 0;
        }
    }

    /**
     * read orientation from Image file
     *
     * @param file File
     * @return Orientation
     */
    private static int getOrientation(String file) {
        int orientation = 1;
        ExifInterface exif;
        if (!TextUtils.isEmpty(file)) {
            try {
                exif = new ExifInterface(file);
                orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            } catch (IOException | ExceptionInInitializerError e) {
                e.printStackTrace();
            }
        }
        return orientation;
    }

    /**
     * Deletes directory and its contents - all
     * Use with caution
     *
     * @param fileOrDirectory File or Directory
     */
    public static void deleteDirectoryAndContents(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {
                deleteDirectoryAndContents(child);
            }
        }

        fileOrDirectory.delete();
    }

    /**
     * Number of MBs available
     *
     * @param file File
     * @return Float
     */
    public static float megabytesAvailable(File file) {
        StatFs stat = new StatFs(file.getPath());
        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getAvailableBlocks();
        return bytesAvailable / (1024.f * 1024.f);
    }

}