package co.phnetworks.veteran.admin.domain;

/**
 * Created by mohamed on 4/4/16.
 */
public class AdminMessageHolder {

    private MessageDetails messageDetails;
    private VeteranDetail veteranDetail;


    public VeteranDetail getVeteranDetail() {
        return veteranDetail;
    }

    public void setVeteranDetail(VeteranDetail veteranDetail) {
        this.veteranDetail = veteranDetail;
    }

    public MessageDetails getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(MessageDetails messageDetails) {
        this.messageDetails = messageDetails;
    }


    @Override
    public String toString() {
        return "AdminMessageHolder{" +
                "messageDetails=" + messageDetails +
                ", veteranDetail=" + veteranDetail +
                '}';
    }
}
