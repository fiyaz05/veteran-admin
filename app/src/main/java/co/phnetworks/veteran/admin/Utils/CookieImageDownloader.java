package co.phnetworks.veteran.admin.Utils;

import android.content.Context;
import android.net.Uri;

import com.squareup.picasso.UrlConnectionDownloader;
import java.io.IOException;
import java.net.HttpURLConnection;

public  class CookieImageDownloader extends UrlConnectionDownloader{

    public CookieImageDownloader(Context context) {
        super(context);
    }


    @Override
    protected HttpURLConnection openConnection(Uri path) throws IOException{
        HttpURLConnection conn = super.openConnection(path);
        String cookie = new SessionService().getSession();
        conn.setRequestProperty("Cookie",cookie);

        return conn;
    }
}

