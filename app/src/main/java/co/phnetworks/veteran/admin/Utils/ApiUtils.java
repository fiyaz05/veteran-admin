package co.phnetworks.veteran.admin.Utils;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import co.phnetworks.veteran.admin.App;
import co.phnetworks.veteran.admin.ApplicationConstants;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;
import co.phnetworks.veteran.admin.domain.ApplicationInstallation;
import co.phnetworks.veteran.admin.domain.MessageDetails;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by mohamed on 6/4/16.
 */
public class ApiUtils {

    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");


    public static String GcmRegisteration(ApplicationInstallation installation) throws IOException, NotSignedInException, SessionExpiredException {

        String url = App.BASE_URL + App.GCM_REGISTRATION;
        Gson gson = new GsonBuilder().create();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(JSON, gson.toJson(installation)))
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (!response.isSuccessful()) {
            throw new NotSignedInException();
        }


        if (response.isSuccessful()) {
            String body = response.body().string();

            if (TextUtils.isEmpty(body)) {
                return null;
            }
            return body;
        }

        if (response.code() == 401) {
            throw new SessionExpiredException();
        }

        return null;
    }

    public static boolean login(OkHttpClient ok, String username, String password) throws IOException, ForbiddenException, InvalidInputException {

        String url = App.BASE_URL + App.LOGIN_AS_ADMIN;

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            return false;
        }

        HashMap<String, String> authenticatedMap = new HashMap<>();
        authenticatedMap.put("username", username);
        authenticatedMap.put("password", password);

        RequestBody formBody = new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = ok.newCall(request).execute();

        if (response.code() == 200) {
            if (request.body().toString().equals(String.valueOf(ApplicationConstants.CLIENT_TYPE))) {
                throw new InvalidInputException();
            }
            String cookie = response.header("Set-Cookie");
            if (TextUtils.isEmpty(cookie)) {
                return false;
            }

            cookie = cookie.substring(0, cookie.indexOf(";"));
            new SessionService().saveSession(cookie);

            return true;
        }

        if (response.code() == 403) {
            throw new ForbiddenException();
        }

        return false;
    }

    public static ArrayList<AdminMessageHolder> downloadDashboardDetails(String url, int limit) throws IOException, NotSignedInException, SessionExpiredException {

        ArrayList<AdminMessageHolder> recycleViewlistObject = new ArrayList<>();
        String cookie = new SessionService().getSession();

        if (TextUtils.isEmpty(cookie)) {
            throw new NotSignedInException();
        }
        Gson gson = new GsonBuilder().create();

        Request request = new Request.Builder()
                .url(url + "?limit=" + limit)
                .get()
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {
            String body = response.body().string();

            AdminMessageHolder[] databasequerys = gson.fromJson(body, AdminMessageHolder[].class);
            for (AdminMessageHolder databasequery : databasequerys) {
                recycleViewlistObject.add(databasequery);
            }

            if (TextUtils.isEmpty(body)) {
                return null;
            }
            return recycleViewlistObject;
        }

        if (response.code() == 401) {
            throw new SessionExpiredException();
        }

        return null;
    }

    public static ArrayList<MessageDetails> downloadConverstion(String url, long veteranid) throws IOException, NotSignedInException, SessionExpiredException {

        ArrayList<MessageDetails> messageDetailsArrayList = new ArrayList<>();
        String cookie = new SessionService().getSession();

        if (TextUtils.isEmpty(cookie)) {
            throw new NotSignedInException();
        }

        Gson gson = new GsonBuilder().create();

        Request request = new Request.Builder()
                .url(url + "?veteranid=" + veteranid)
                .get()
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {

            String body = response.body().string();
            MessageDetails[] messageDetailses = gson.fromJson(body, MessageDetails[].class);
            for (MessageDetails messageDetails : messageDetailses) {
                messageDetailsArrayList.add(messageDetails);
            }

            if (TextUtils.isEmpty(body)) {
                return null;
            }

            return messageDetailsArrayList;
        }


        if (response.code() == 401) {

            throw new SessionExpiredException();
        }

        return null;

    }

    public static ArrayList<MessageDetails> downloadBroadcastConverstion(String url) throws IOException, NotSignedInException, SessionExpiredException {

        ArrayList<MessageDetails> messageDetailsArrayList = new ArrayList<>();
        String cookie = new SessionService().getSession();

        if (TextUtils.isEmpty(cookie)) {
            throw new NotSignedInException();
        }

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {

            String body = response.body().string();
            Gson gson = new GsonBuilder().create();
            MessageDetails[] messageDetailses = gson.fromJson(body, MessageDetails[].class);

            for (MessageDetails messageDetails : messageDetailses) {
                messageDetailsArrayList.add(messageDetails);
            }

            if (TextUtils.isEmpty(body)) {
                return null;
            }

            return messageDetailsArrayList;
        }


        if (response.code() == 401) {
            throw new SessionExpiredException();
        }

        return null;

    }

    public static String sendAdminReply(String url, MessageDetails messageDetails) throws IOException, NotSignedInException, SessionExpiredException {

        String cookie = new SessionService().getSession();
        Gson gson = new GsonBuilder().create();

        if (TextUtils.isEmpty(cookie)) {
            throw new NotSignedInException();
        }

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(JSON, gson.toJson(messageDetails)))
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {
            String body = response.body().string();
            if (TextUtils.isEmpty(body)) {
                return null;
            }
            return body;
        }

        if (response.code() == 401) {
            throw new SessionExpiredException();
        }

        return null;
    }

    public static boolean updateMarkStatus (String url, long messageid, int markStatus) throws IOException, NotSignedInException, SessionExpiredException {

        String cookie = new SessionService().getSession();

        if (TextUtils.isEmpty(cookie)) {
            throw new NotSignedInException();
        }

        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("messageid", String.valueOf(messageid))
                .add("markstatus", String.valueOf(markStatus));

        RequestBody formBody = formBuilder.build();

        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();

        Response response = App.getApp().getOk().newCall(request).execute();

        if (response.isSuccessful()) {

            String body = response.body().string();

            if (body.equals("true")) {
                return true;
            } else {
                return false;
            }
        }

        if (response.code() == 401) {

            throw new SessionExpiredException();
        }

        return false;
    }


}
