package co.phnetworks.veteran.admin.Utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;

/**
 * Created by Seenivasan on 4/5/16.
 */
public class ApplicationUtils {

    private static final int VALID = 0;
    private static final int INVALID = 1;
    private static final String SIGNATURE = "GYee7P/V/TUJg7BDWq8IXPYsMMA=";

    public static boolean checkAppSignature(Context context) {

        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {

                byte[] signatureBytes = signature.toByteArray();
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                final String currentSignature = Base64.encodeToString(md.digest(), Base64.DEFAULT);

                if (SIGNATURE.equals(currentSignature.trim())) {
                    return true;
                }
            }

        } catch (Exception e) {
        }
        return false;
    }
}
