package co.phnetworks.veteran.admin.common.http;

import java.io.IOException;
import java.util.HashMap;

/**
 * Author: Seenivasan
 * Date: 11/24/2015
 * Since: 1.0.0
 */
public interface IHttpService {

    String postHttpRequestOnLogin(String url, HashMap<String, String> parameters) throws IOException;
}

