package co.phnetworks.veteran.admin;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.MessageDetails;
import co.phnetworks.veteran.admin.domain.VeteranDetail;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;

public class VeteranMessageActivity extends AppCompatActivity implements TextWatcher {

    private static final int SUCCESS = 1;
    private static final int INTERNET_DISCONNECTED = 2;
    private static final int LOGIN_FAILED = 3;
    private static final int FAILED = 4;
    public static boolean screenAlive = false;
    private EditText replybox;
    private ImageButton replybutton;
    private Button skipButton;
    private CheckBox markCheckbox;
    private ProgressDialog progressBar;
    private RecyclerView threadview;
    private ConversationAdapterView adapter;
    private RecyclerView.LayoutManager layoutManager;
    private int threadcount = 0;
    private RelativeLayout internetConnectionLayout;
    private TextView message;
    private Button retry;
    private ActionBar mActionBar;
    private VeteranDetail veteran;
    private MessageDetails lastMessageofConversation;
    private HomeActivity homeActivity;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            if (action.equals("GCM-Notification")) {
                if (screenAlive) {
                    runAsyntaskForMessageConveration(false);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_veteran_message);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String veteranString = extras.getString("veteran");
            Type type = new TypeToken<VeteranDetail>() {
            }.getType();
            veteran = new Gson().fromJson(veteranString, type);
            //The key argument here must match that used in the other activity
        }

        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle(veteran.getName());
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(true);
            mActionBar.setDisplayUseLogoEnabled(true);
        }

        internetConnectionLayout = (RelativeLayout) findViewById(R.id.internetconnection);
        markCheckbox = (CheckBox) findViewById(R.id.markstatus);
        message = (TextView) findViewById(R.id.noconnectionmessage);
        retry = (Button) findViewById(R.id.nointernetbutton);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runAsyntaskForMessageConveration(true);
            }
        });

        threadview = (RecyclerView) findViewById(R.id.thread_view);
        threadview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        threadview.setLayoutManager(layoutManager);
        replybox = (EditText) findViewById(R.id.et_message);
        replybox.setSelection(0);
        replybutton = (ImageButton) findViewById(R.id.btn_send);
        skipButton = (Button) findViewById(R.id.skip);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        replybox.addTextChangedListener(this);

        replybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyValidation();
            }
        });

        replybox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (threadcount > 0) {
                            threadview.getLayoutManager().smoothScrollToPosition(threadview, null, threadview.getAdapter().getItemCount());
                        }
                    }
                }, 100);
                return false;
            }
        });

        markCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b) {
                    if (TextUtils.isEmpty(replybox.getText().toString())) {
                        replybutton.setVisibility(View.INVISIBLE);
                        skipButton.setVisibility(View.VISIBLE);
                    } else {
                        skipButton.setVisibility(View.INVISIBLE);
                        replybutton.setVisibility(View.VISIBLE);
                    }
                } else {
                    skipButton.setVisibility(View.INVISIBLE);
                    replybutton.setVisibility(View.VISIBLE);
                }
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                long messageId = lastMessageofConversation.getId();

                final AsyncMarkStatus asyncMarkStatus = new AsyncMarkStatus(messageId, ApplicationConstants.MARKED);
                asyncMarkStatus.execute();
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("GCM-Notification"));
        runAsyntaskForMessageConveration(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        screenAlive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        screenAlive = false;
    }

    private void setupUI(final ArrayList<MessageDetails> messageDetailsArrayList) {
        if (adapter == null) {
            adapter = new ConversationAdapterView(this, messageDetailsArrayList);
            threadview.setAdapter(adapter);
        } else {
            adapter.setDataset(messageDetailsArrayList);
            adapter.notifyDataSetChanged();
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (messageDetailsArrayList.get(messageDetailsArrayList.size()-1).getMarkstatus() == 1) {
                    markCheckbox.setChecked(true);
                    replybutton.setVisibility(View.INVISIBLE);
                    skipButton.setVisibility(View.VISIBLE);
                }else {
                    markCheckbox.setChecked(false);
                    skipButton.setVisibility(View.INVISIBLE);
                    replybutton.setVisibility(View.VISIBLE);
                }
                if (threadcount > 0) {
                    threadview.getLayoutManager().smoothScrollToPosition(threadview, null, threadview.getAdapter().getItemCount());
                }
            }
        }, 10);

    }

    private void replyValidation() {
        if (!TextUtils.isEmpty(replybox.getText().toString())) {
            UiUtils.hideKeyboard(this, replybox);
            runAsyntaskForSendMessage();
        } else {
            UiUtils.showToast(getResources().getString(R.string.empty_query));
        }
    }

    public void progressdialog(String message) {
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage(message);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
    }

    private void runAsyntaskForMessageConveration(boolean visibilty) {

        if (GenericUtils.isInternetAvailable(this)) {

            final AsyncGetMessageThread asyncGetMessageThread = new AsyncGetMessageThread(veteran.getId(), visibilty);
            asyncGetMessageThread.execute();
        } else {

            UiUtils.showToast(getResources().getString(R.string.no_internet));
        }

    }

    private void runAsyntaskForSendMessage() {
        if (GenericUtils.isInternetAvailable(this)) {

            final AsyncSendData asyncSendData = new AsyncSendData(replybox.getText().toString());
            asyncSendData.execute();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (asyncSendData.getStatus() == AsyncTask.Status.RUNNING) {
                        asyncSendData.cancel(true);
                        progressBar.cancel();
                        UiUtils.hideKeyboard(VeteranMessageActivity.this, replybox);
                        UiUtils.showToast(getResources().getString(R.string.slow_connection));
                    }
                }
            }, 8000);

        } else {

            UiUtils.showToast(getResources().getString(R.string.no_internet));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        long messageId = lastMessageofConversation.getId();

        if (markCheckbox.isChecked()) {
            final AsyncMarkStatus asyncMarkStatus = new AsyncMarkStatus(messageId, ApplicationConstants.MARKED);
            asyncMarkStatus.execute();
        } else {
            final AsyncMarkStatus asyncMarkStatus = new AsyncMarkStatus(messageId, ApplicationConstants.UNMARKED);
            asyncMarkStatus.execute();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (markCheckbox.isChecked()) {
            if (TextUtils.isEmpty(editable.toString())) {
                replybutton.setVisibility(View.INVISIBLE);
                skipButton.setVisibility(View.VISIBLE);
            } else {
                skipButton.setVisibility(View.INVISIBLE);
                replybutton.setVisibility(View.VISIBLE);
            }
        }
    }

    class AsyncGetMessageThread extends AsyncTask<Void, Void, Integer> {

        protected long veteranid;
        protected boolean dialogvisibilty;
        protected int count = 0;
        ArrayList<MessageDetails> mobject;

        protected AsyncGetMessageThread(long id, boolean visibilty) {
            dialogvisibilty = visibilty;
            veteranid = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialogvisibilty) {
                progressdialog(getResources().getString(R.string.loading));
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.GET_ALL_CONVERSATION;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed && ++count < 5) {
                try {
                    mobject = ApiUtils.downloadConverstion(url, veteranid);
                    success = (mobject != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {

            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(VeteranMessageActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);
                if (mobject != null) {
                    if (mobject.size() > 0) {
                        lastMessageofConversation = mobject.get(mobject.size()-1);
                        setupUI(mobject);
                        threadcount = mobject.size();
                    }
                }
            }

            if (FAILED == result) {
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    class AsyncSendData extends AsyncTask<Void, Void, Integer> {

        protected String response = null;
        protected String answer;
        protected MessageDetails messageDetails;

        protected AsyncSendData(String reply) {
            answer = reply;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            super.onPreExecute();
            messageDetails = new MessageDetails();
            messageDetails.setMessage(answer);

            if (veteran != null) {
                messageDetails.setToid(veteran.getId());
            } else {
                messageDetails.setBroadcast(1);
            }

            if (markCheckbox.isChecked()) {
                messageDetails.setMarkstatus(1);
            }

            progressdialog(getResources().getString(R.string.message_sending));
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.SEND_ADMIN_REPLY;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed) {
                try {
                    response = ApiUtils.sendAdminReply(url, messageDetails);

                    success = (response != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {
            // dismiss the dialog after the file was downloaded
            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(VeteranMessageActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);
                if (Boolean.parseBoolean(response)) {
                    runAsyntaskForMessageConveration(false);
                    replybox.setText("");
                }
            }

            if (FAILED == result) {
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
//                GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),context);
            }
        }
    }

    class AsyncMarkStatus extends AsyncTask<Void, Void, Integer> {

        protected boolean response;
        protected long messageId;
        protected int status;

        protected AsyncMarkStatus(long mMessageId, int markstatus) {
            messageId = mMessageId;
            status = markstatus;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            super.onPreExecute();

            progressdialog(getResources().getString(R.string.loading));
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.UPDATE_MARK_STATUS;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed) {
                try {
                    response = ApiUtils.updateMarkStatus(url, messageId, status);

                    success = (response);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {
            // dismiss the dialog after the file was downloaded
            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(VeteranMessageActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);

                Intent intent = new Intent("ReloadingView");
                intent.putExtra("veteranId", veteran.getId());
                intent.putExtra("markstatus", status);
                if (homeActivity.getCurrentTab() == 0) {
                    intent.putExtra("seenstatus", System.currentTimeMillis());
                } else {
                    intent.putExtra("seenstatus", 0);
                }
                LocalBroadcastManager.getInstance(App.getApp()).sendBroadcast(intent);

                finish();
            }

            if (FAILED == result) {
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
//                GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),context);
            }
        }
    }


}
