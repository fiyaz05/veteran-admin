package co.phnetworks.veteran.admin.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import co.phnetworks.veteran.admin.App;


/**
 * Created by Seenivasan on 1/4/16.
 */
public class SessionService {

    private static final String SETTINGS_NAME = "veteranportal.settings";
    private static final String SESSION = "session";

    public static void saveSession(String sessionId) {
        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(SESSION, sessionId);
        editor.commit();
    }

    public static String getSession() {
        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        String sessionId = preferences.getString(SESSION, null);
        if (TextUtils.isEmpty(sessionId)) {
            return null;
        }
        return sessionId;
    }

    public static void clearSession() {
        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        String sessionId = preferences.getString(SESSION, null);
        if (!TextUtils.isEmpty(sessionId)) {
            preferences.edit().remove(SESSION).commit();
        }
    }
}
