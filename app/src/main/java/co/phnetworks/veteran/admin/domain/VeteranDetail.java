package co.phnetworks.veteran.admin.domain;

/**
 * Created by mohamed on 24/3/16.
 */
public class VeteranDetail {
    private long id;
    private long accountid;
    private String servicenumber;
    private String phonenumber;
    private String name;
    private int rank;
    private int regtcorps;
    private int type;
    private String address;
    private String emailaddress;
    private long created;
    private String profilepic;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getServicenumber() {
        return servicenumber;
    }

    public void setServicenumber(String servicenumber) {
        this.servicenumber = servicenumber;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getEmailaddress() {
        return emailaddress;
    }

    public void setEmailaddress(String emailaddress) {
        this.emailaddress = emailaddress;
    }

    public int getRegtcorps() {
        return regtcorps;
    }

    public void setRegtcorps(int regtcorps) {
        this.regtcorps = regtcorps;
    }

    public long getAccountid() {
        return accountid;
    }

    public void setAccountid(long accountid) {
        this.accountid = accountid;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    @Override
    public String toString() {
        return "VeteranDetail{" +
                "id=" + id +
                ", accountid=" + accountid +
                ", servicenumber='" + servicenumber + '\'' +
                ", phonenumber='" + phonenumber + '\'' +
                ", name='" + name + '\'' +
                ", rank=" + rank +
                ", regtcorps=" + regtcorps +
                ", type=" + type +
                ", address='" + address + '\'' +
                ", emailaddress='" + emailaddress + '\'' +
                ", created=" + created +
                ", profilepic='" + profilepic + '\'' +
                '}';
    }
}
