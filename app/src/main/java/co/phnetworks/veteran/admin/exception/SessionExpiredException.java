package co.phnetworks.veteran.admin.exception;

public class SessionExpiredException extends Exception {

    @Override
    public String getMessage() {
        return "Session expired";
    }
}