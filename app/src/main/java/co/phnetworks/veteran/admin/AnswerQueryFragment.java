package co.phnetworks.veteran.admin;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.MessageDetails;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;

/**
 * Created by mohamed on 19/3/16.
 */
public class AnswerQueryFragment extends Fragment {


    private EditText replybox;
    private ImageButton replybutton;
    private ProgressDialog progressBar;
    private Long threadviewindicator = null;
    private RecyclerView threadview;
    private ConversationAdapterView adapter;
    private RecyclerView.LayoutManager layoutManager;
    private int threadcount = 0;
    private RelativeLayout internetConnectionLayout;
    private TextView message;
    private Button retry;

    private static final int SUCCESS = 1;
    private static final int INTERNET_DISCONNECTED = 2;
    private static final int LOGIN_FAILED = 3;
    private static final int FAILED = 4;

    public static boolean screenAlive = false;

    private Context context = null;

    public AnswerQueryFragment(){

    }
    @SuppressLint("ValidFragment")
    public AnswerQueryFragment(Long extra) {
        super();
        threadviewindicator = extra;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this.getActivity().getWindow().getContext();

        if(threadviewindicator != null){
           runAsyntaskForMessageConveration(true);
        }else {
           runAsyntaskForBroadcastConveration(true);
        }

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter("GCM-Notification"));
    }

    @Override
    public void onStart() {
        super.onStart();
        screenAlive = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        screenAlive = false;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();
            if (action.equals("GCM-Notification")) {

                if (screenAlive) {
                    if(threadviewindicator != null){
                        runAsyntaskForMessageConveration(false);
                    }else {
                        runAsyntaskForBroadcastConveration(false);
                    }
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       return inflater.inflate(R.layout.layout_reply_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        internetConnectionLayout = (RelativeLayout) view.findViewById(R.id.internetconnection);
        message = (TextView) view.findViewById(R.id.noconnectionmessage);
        retry = (Button) view.findViewById(R.id.nointernetbutton);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(threadviewindicator != null){
                    runAsyntaskForMessageConveration(true);
                }else {
                    runAsyntaskForBroadcastConveration(true);
                }
            }
        });
        threadview = (RecyclerView) view.findViewById(R.id.thread_view);
        threadview.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        threadview.setLayoutManager(layoutManager);
        replybox = (EditText) view.findViewById(R.id.et_message);
        replybox.requestFocus();
        replybox.setSelection(0);
        replybutton = (ImageButton) view.findViewById(R.id.btn_send);

        replybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                replyValidation();
            }
        });


        replybox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (threadcount > 0) {
                            threadview.getLayoutManager().smoothScrollToPosition(threadview, null, threadview.getAdapter().getItemCount());
                        }
                    }
                }, 100);
                return false;
            }
        });

    }

    private void setupUI(ArrayList<MessageDetails> messageDetailsArrayList) {

        if( adapter == null) {
            adapter = new ConversationAdapterView(context, messageDetailsArrayList);
            threadview.setAdapter(adapter);
        }else {
            adapter.setDataset(messageDetailsArrayList);
            adapter.notifyDataSetChanged();
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (threadcount > 0) {
                    threadview.getLayoutManager().smoothScrollToPosition(threadview, null, threadview.getAdapter().getItemCount());
                }
            }
        }, 10);

    }

    private void replyValidation(){
        if (!TextUtils.isEmpty(replybox.getText().toString())){
            UiUtils.hideKeyboard(context, replybox);
            runAsyntaskForSendMessage();
        }
        else {
            UiUtils.showToast(getResources().getString(R.string.empty_query));
        }
    }

    public void progressdialog(String message) {
        progressBar = new ProgressDialog(context);
        progressBar.setCancelable(false);
        progressBar.setMessage(message);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
    }

    private void runAsyntaskForMessageConveration(boolean visibilty){

        if (GenericUtils.isInternetAvailable(context)) {

            final AsyncGetMessageThread asyncGetMessageThread = new AsyncGetMessageThread(threadviewindicator,visibilty);
            asyncGetMessageThread.execute();
        } else {

            UiUtils.showToast(getResources().getString(R.string.no_internet));
        }

    }

    private void runAsyntaskForBroadcastConveration(boolean visibilty){

        if (GenericUtils.isInternetAvailable(context)) {

            final AsyncGetBroadcastMessageThread asyncGetBroadcastMessageThread= new AsyncGetBroadcastMessageThread(visibilty);
            asyncGetBroadcastMessageThread.execute();
        } else {

            UiUtils.showToast(getResources().getString(R.string.no_internet));
        }

    }

    private void runAsyntaskForSendMessage(){
        if (GenericUtils.isInternetAvailable(context)) {

            final AsyncSendData asyncSendData = new AsyncSendData(replybox.getText().toString());
            asyncSendData.execute();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable()
            {
                @Override
                public void run() {
                    if ( asyncSendData.getStatus() == AsyncTask.Status.RUNNING ) {
                        asyncSendData.cancel(true);
                        progressBar.cancel();
                        UiUtils.hideKeyboard(context, replybox);
                        UiUtils.showToast(getResources().getString(R.string.slow_connection));
                    }
                }
            },8000 );

        } else {

            UiUtils.showToast(getResources().getString(R.string.no_internet));
        }
    }


    class AsyncGetMessageThread extends AsyncTask<Void, Void, Integer> {

        ArrayList<MessageDetails> mobject;
        protected long veteranid;
        protected boolean dialogvisibilty;
        protected int count = 0;

        protected AsyncGetMessageThread(long id, boolean visibilty){
            dialogvisibilty = visibilty;
            veteranid = id;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialogvisibilty) {
                progressdialog(getResources().getString(R.string.loading));
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.GET_ALL_CONVERSATION;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed && ++count < 5) {
                try {
                    mobject= ApiUtils.downloadConverstion(url,veteranid);
                    success = (mobject != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {

            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(context, LoginActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();

                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);
                if ( mobject != null) {
                    if (mobject.size() > 0) {
                        setupUI(mobject);
                        threadcount = mobject.size();
                    }
                }
            }

            if(FAILED == result){
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
                //GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),context);
            }
        }
    }

    class AsyncGetBroadcastMessageThread extends AsyncTask<Void, Void, Integer> {

        ArrayList<MessageDetails> mobject;
        boolean dialogvisibilty ;
        int count = 0;

        protected AsyncGetBroadcastMessageThread(boolean visibilty){
            dialogvisibilty = visibilty;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialogvisibilty) {
                progressdialog(getResources().getString(R.string.loading));
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.GET_ALL_BROADCAST_CONVERSATION;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed && ++count < 5) {
                try {
                    mobject= ApiUtils.downloadBroadcastConverstion(url);

                    success = (mobject != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {

            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(context, LoginActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();

                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);
                if ( mobject != null) {
                    if (mobject.size() > 0) {
                        setupUI(mobject);
                        threadcount = mobject.size();
                    }
                }
            }

            if (FAILED == result){
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
                //GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),context);
            }
        }
    }

    class AsyncSendData extends AsyncTask<Void, Void, Integer> {

        protected  String response = null;
        protected String answer;
        protected MessageDetails messageDetails;

        protected AsyncSendData(String reply){
            answer = reply;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            super.onPreExecute();
            messageDetails = new MessageDetails();
            messageDetails.setMessage(answer);
            if(threadviewindicator != null){
                messageDetails.setToid(threadviewindicator);
            }else {
                messageDetails.setBroadcast(1);
            }

            progressdialog(getResources().getString(R.string.message_sending));
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.SEND_ADMIN_REPLY;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed) {
                try {
                    response = ApiUtils.sendAdminReply(url, messageDetails);

                    success = (response != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {

                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }

            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;

        }

        @Override
        protected void onPostExecute(Integer result) {
            // dismiss the dialog after the file was downloaded
            progressBar.cancel();
            if (LOGIN_FAILED == result) {

                Intent intent = new Intent(context, LoginActivity.class);
                getActivity().startActivity(intent);
                getActivity().finish();
                return;
            }

            if (SUCCESS == result) {
                internetConnectionLayout.setVisibility(View.GONE);
                if(Boolean.parseBoolean(response)){
                    if(threadviewindicator != null){
                        runAsyntaskForMessageConveration(false);
                    }else {
                        runAsyntaskForBroadcastConveration(false);
                    }
                    //UiUtils.showToast(getResources().getString(R.string.message_sent));
                    replybox.setText("");
                }
            }

            if (FAILED == result){
                message.setText(getResources().getString(R.string.servernotreachable));
                internetConnectionLayout.setVisibility(View.VISIBLE);
//                GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),context);
            }
        }
    }
}
