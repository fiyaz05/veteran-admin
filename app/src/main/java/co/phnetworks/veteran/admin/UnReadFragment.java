package co.phnetworks.veteran.admin;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;


public class UnReadFragment extends Fragment {

    // private List<AdminMessageHolder> messageHolders = new ArrayList<>();
    private List<AdminMessageHolder> unReadMessages = new ArrayList<>();
    private RecyclerView recyclerView;
    private UnReadListAdapter adapter;
    private TextView no_internet, message;

    //private OnFragmentInteractionListener mListener;
    private HomeActivity homeActivity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();
        /*if (extras != null) {
            currentStatus = extras.getInt("currentStatus");
        }*/
        unReadMessages = new ArrayList<>();
    }

    private void populateData() {
        unReadMessages.clear();
        homeActivity = (HomeActivity) getActivity();
        if (homeActivity.getMessageHolderList() != null) {
            for (AdminMessageHolder messageHolder : homeActivity.getMessageHolderList()) {
                if (messageHolder.getMessageDetails().getSeen() == null) {
                    if (messageHolder.getMessageDetails().getFromid() != 1) {
                        unReadMessages.add(messageHolder);
                    }
                }
            }
        }
        loadUI();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_un_read, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.unread_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        no_internet = (TextView) view.findViewById(R.id.no_internet);
        message = (TextView) view.findViewById(R.id.message);
        populateData();
    }

    private void loadUI() {
        adapter = new UnReadListAdapter(getContext(), unReadMessages);

        if (!GenericUtils.isInternetAvailable(getActivity())) {
            unReadMessages.clear();
            no_internet.setVisibility(View.VISIBLE);

        } else if (unReadMessages != null) {

            adapter.setDataset(unReadMessages);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            if (unReadMessages.size() == 0) {
                message.setText(getResources().getString(R.string.no_result_found));
                message.setVisibility(View.VISIBLE);
            }

            adapter.SetOnItemClickListener(new UnReadListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View v, AdminMessageHolder adminMessageHolder, int position) {
                    homeActivity.setReload(false);
                    Intent intent = new Intent(getActivity(), VeteranMessageActivity.class);
                    Gson gson = new Gson();
                    intent.putExtra("veteran", gson.toJson(adminMessageHolder.getVeteranDetail()));
                    startActivity(intent);
                }
            });

            adapter.SetOnProfileClickListener(new UnReadListAdapter.OnProfileClickListener() {
                @Override
                public void oncontactpicClick(View view, AdminMessageHolder adminMessageHolder) {
                    UiUtils.profileWidget(adminMessageHolder, getActivity());
                }
            });

            adapter.notifyDataSetChanged();

        } else {
            message.setText(getResources().getString(R.string.no_result_found));
            message.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
  /*  public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    /*public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}


