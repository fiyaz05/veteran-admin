package co.phnetworks.veteran.admin.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import android.text.TextUtils;
import android.util.Base64;

import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.Entity;
import com.facebook.crypto.exception.CryptoInitializationException;
import com.facebook.crypto.exception.KeyChainException;
import com.facebook.crypto.util.SystemNativeCryptoLibrary;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import co.phnetworks.veteran.admin.App;
import co.phnetworks.veteran.admin.domain.ApplicationInstallation;

/**
 * Created by mohamed on 1/3/16.
 */
public class Datastore  {
    public final static String[] rank = new String[]{"Field Marshal", "General", "Lieutenant General", "Major General", "Brigadier", "Colonel", "Colonel(TS)", "Lieutenant Colonel", "Lieutenant Colonel(TS)", "Major", "Captain", "Lieutenant", "Second Lieutenant", "Subedar Major", "Subedar", "Naib Subedar", "Havildar", "Naik", "Lance Naik", "Sepoy/ Rifleman"};
    public final static String[] typecat = new String[]{"ADC (Med. Services)", "AEC", "AMC (Med. Services)", "AOC", "APS", "APTC", "Armd Corps", "ASC", "Aviation Corps", "Bengal Engr Gp", "Bombay Engr Gp", "Corps of Signal", "DSC", "EME", "General Services", "Int Coprs", "JAG Deptt", "Madras Engr Gp", "Mil Farms Services", "MNS (Med. Services)", "PNR Corps", "Provost", "RVC", "TA"};
    public final static String[] regiment = new String[]{"1 GR", "11 GR", "3 GR", "4 GR", "5 GR", "8 GR", "9 GR", "AAD Regt", "Arty Regt", "Assam", "Bihar", "Dogra", "Dogra Scouts", "Garh Rif", "Garh Scouts", "Grenadiers", "Guards", "JAK Li Inf", "JAK Rif", "JAT", "Kumaon", "Kumaon Scouts", "Ladakh Scouts", "Madras", "Mahar", "Maratha Li Inf", "Mech Inf", "Naga", "Para", "Punjab", "Raj Rif", "Rajput", "Sikh", "Sikh Li Inf"};
    private static final String SETTINGS_NAME = "veteranportal.settings";
    private static final String STATICDATA = "staticdata";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String APPREFERRER = "appreferrer";
    private static final String ADMIN_USERNAME = "admin_user_name";
    private static final String ADMIN_PASSWORD = "admin_password";
    private static ApplicationInstallation sAppRefererInfo;
    /**
     * Use with caution
     */
    public static void clear() {
        SharedPreferences settings = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }

    /**
     * Stores the app referrer locally
     * @param appReferrerInfo AppRefererInfo
     */
    public static void saveAppReferrer(ApplicationInstallation appReferrerInfo) {
        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(APPREFERRER, new GsonBuilder().create().toJson(appReferrerInfo));
        editor.commit();

        sAppRefererInfo = appReferrerInfo;
    }

    /**
     * Gets the locally stored app referrer
     * @return String
     */
    public static ApplicationInstallation getAppReferrer() {

        if (sAppRefererInfo != null) {
            return sAppRefererInfo;
        }

        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        String appRefererString = preferences.getString(APPREFERRER, null);

        if (TextUtils.isEmpty(appRefererString)) {
            return null;
        }

        return new GsonBuilder().create().fromJson(appRefererString, ApplicationInstallation.class);
    }

    /**
     * Stores the static data locally
     * @param data data
     */
    public static void saveData(String data) {
        if( data != null) {
            SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(STATICDATA, data);
            editor.commit();
        }
    }

    /**
     * Gets the locally stored data
     * @return String
     */
    public static String getSaveData() {

        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        return preferences.getString(STATICDATA, null);
    }



    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    public static void storeGCMRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        int appVersion = GenericUtils.getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    public static String getGCMRegistrationId(Context context) {
        final SharedPreferences prefs = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = GenericUtils.getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    public static void saveUserName(String name) {

        Crypto crypto = new Crypto(
                new SharedPrefsBackedKeyChain(App.getApp()),
                new SystemNativeCryptoLibrary());

        try {
            byte[] nameBytes = name.getBytes();
            Entity entity = new Entity(ADMIN_USERNAME);
            byte[] encryptedBytes = crypto.encrypt(nameBytes, entity);
            String encryptedUserString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
            SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(ADMIN_USERNAME, encryptedUserString);
            editor.commit();

        } catch (CryptoInitializationException | IOException | KeyChainException e) {
            e.printStackTrace();
        }


    }

    public static String getUsername() {
        SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);

        String encryptedUserString = preferences.getString(ADMIN_USERNAME, null);
        if (!TextUtils.isEmpty(encryptedUserString)) {

            Crypto crypto = new Crypto(
                    new SharedPrefsBackedKeyChain(App.getApp()),
                    new SystemNativeCryptoLibrary());

            try {
                byte[] encryptedBytes = Base64.decode(encryptedUserString, Base64.NO_WRAP);
                byte[] decryptedBytes = crypto.decrypt(encryptedBytes, new Entity(ADMIN_USERNAME));
                return new String(decryptedBytes);

            } catch (CryptoInitializationException | IOException | KeyChainException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void savePassword(String password) {

         Crypto crypto = new Crypto(
                new SharedPrefsBackedKeyChain(App.getApp()),
                new SystemNativeCryptoLibrary());

        try {
            byte[] passwordBytes = password.getBytes();
            Entity entity = new Entity(ADMIN_PASSWORD);
            byte[] encryptedBytes = crypto.encrypt(passwordBytes, entity);
            String encryptedPasswordString = Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
            SharedPreferences preferences = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(ADMIN_PASSWORD, encryptedPasswordString);
            editor.commit();

        } catch (CryptoInitializationException | IOException | KeyChainException e) {
            e.printStackTrace();
        }
    }

    public static String getPassword() {

        SharedPreferences settings = App.getApp().getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);

        String encryptedDoctorString = settings.getString(ADMIN_PASSWORD, null);
        if (!TextUtils.isEmpty(encryptedDoctorString)) {

            Crypto crypto = new Crypto(
                    new SharedPrefsBackedKeyChain(App.getApp()),
                    new SystemNativeCryptoLibrary());

            try {
                byte[] encryptedBytes = Base64.decode(encryptedDoctorString, Base64.NO_WRAP);
                byte[] decryptedBytes = crypto.decrypt(encryptedBytes, new Entity(ADMIN_PASSWORD));
                return new String(decryptedBytes);
            } catch (CryptoInitializationException | IOException | KeyChainException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
