package co.phnetworks.veteran.admin;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import co.phnetworks.veteran.admin.Utils.AuthInterceptor;
import co.phnetworks.veteran.admin.Utils.CookieImageDownloader;
import okhttp3.OkHttpClient;

public class App extends Application {

    public static final String GCM_REGISTRATION = "installation/registeradmin";
    public static final String LOGIN_AS_ADMIN = "login";
    public static final String GET_ALL_MESSAGE = "message/admin";
    public static final String GET_ALL_CONVERSATION = "message/conversation";
    public static final String SEND_ADMIN_REPLY = "message/admin";
    public static final String UPDATE_MARK_STATUS = "message/markstatus";
    public static final String GET_ALL_BROADCAST_CONVERSATION = "message/broadcast";
    public static final String GET_PROFILE_PICTURE = "profile/picture?veteranid=";
    public static final String GCM_SENDER_ID = "44811018121";
    public static final String BASE_URL = isProduction() ?
            "https://outreach.phnetworks.co.in/" :
            "http://vetdev.phnetworks.co.in/";
            //"http://192.168.42.219:8080/";
    private static App sApp;
    private Picasso mPicasso;
    private OkHttpClient okclient;

    public static App getApp() {
        return sApp;
    }

    public static boolean isProduction() {
        return BuildConfig.FLAVOR.equals("prod");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (!isProduction()) {
            Stetho.initializeWithDefaults(this);
        }

        mPicasso = new Picasso.Builder(this)
                .downloader(new CookieImageDownloader(this)).build();

        okclient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .addInterceptor(new AuthInterceptor())
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();


        //HttpRequest.setConnectionFactory(new OkConnectionFactory());
        sApp = this;
       // LeakCanary.install(this);
    }

    public Picasso getPicasso() {
        return mPicasso;
    }

    public OkHttpClient getOk() {
        return okclient;
    }
}
