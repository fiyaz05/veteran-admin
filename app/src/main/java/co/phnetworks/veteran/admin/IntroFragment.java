package co.phnetworks.veteran.admin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class IntroFragment extends Fragment {
	
	private int mPage;
	
	private ImageView mImageView;
	private TextView mTitleView;
	private TextView mSubtitleView;
	private Button mGotitButton;
	
	/*private int[] mImageIds = {
		R.drawable.peach_small,
		R.drawable.intro1,
		R.drawable.intro3,
		R.drawable.intro2
	};*/
	
	private int[] mColors = {
		android.R.color.white,
		R.color.intro1,
		R.color.intro2,
		R.color.intro3
	};
	
	private String[] mTitles = App.getApp().getResources().getStringArray(R.array.intro_title_array);
	private String[] mSubTitles = App.getApp().getResources().getStringArray(R.array.intro_subtitle_array);

	public IntroFragment() {
	}
	
	public static IntroFragment getInstance(int page) {

		IntroFragment fragment = new IntroFragment();

		Bundle bundle = new Bundle();
		bundle.putInt("page", page);

		fragment.setArguments(bundle);

		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle bundle = getArguments();
		if (bundle != null) {
			mPage = bundle.getInt("page", 1);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.demo_fragment_layout, container, false);
		
		mImageView = (ImageView) view.findViewById(R.id.image);
		mTitleView = (TextView) view.findViewById(R.id.title);
		mSubtitleView = (TextView) view.findViewById(R.id.subtitle);
		mGotitButton = (Button) view.findViewById(R.id.got_it);
		
		view.setBackgroundColor(App.getApp().getResources().getColor(mColors[mPage]));
		//mImageView.setImageResource(mImageIds[mPage]);
		mTitleView.setText(mTitles[mPage]);
		mSubtitleView.setText(mSubTitles[mPage]);
		
		if (mPage == 1) {
			mGotitButton.setVisibility(View.VISIBLE);
		}
		
		mGotitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				/*
				Intent intent = new Intent(getActivity(), LandingActivity.class);
				getActivity().startActivity(intent);
				getActivity().finish();*/
				IntroActivity introActivity = (IntroActivity) getActivity();
				if(introActivity != null){
					introActivity.redirectToIntroActivity();
				}
			}
		});
		
		return view;
	}
}
