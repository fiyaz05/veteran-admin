package co.phnetworks.veteran.admin;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.common.gcm.service.GcmRegistrationIntentService;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;

public class HomeActivity extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce = false;
    private static final int SUCCESS = 1;
    private static final int INTERNET_DISCONNECTED = 2;
    private static final int LOGIN_FAILED = 3;
    private static final int FAILED = 4;
    private static final int NOQUERIES = 5;
    public static boolean appOnOff = false;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressDialog progressBar;
    private LinearLayout parLinearLayout;
    private List<AdminMessageHolder> messageHolderList = new ArrayList<>();
    private static boolean isReload = true;
    private static int currentTab = 0;

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("GCM-Notification")) {
                new AsyncGetAllMessage(false).execute();
            }

            if (action.equals("ReloadingView")) {

                Long veteranid = intent.getLongExtra("veteranId", 0L);
                int status = intent.getIntExtra("markstatus", 0);
                Long seenstatus = intent.getLongExtra("seenstatus", 0L);


                for(int i=0;i<messageHolderList.size();i++) {
                    if (messageHolderList.get(i).getVeteranDetail().getId() == veteranid) {
                        messageHolderList.get(i).getMessageDetails().setMarkstatus(status);
                        if (seenstatus != 0) {
                            messageHolderList.get(i).getMessageDetails().setSeen(seenstatus);
                        }
                        loadData();
                    }
                }
            }
        }
    };


    public List<AdminMessageHolder> getMessageHolderList() {
        return messageHolderList;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isReload){
            new AsyncGetAllMessage(true).execute();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        parLinearLayout = (LinearLayout) findViewById(R.id.parentlayout);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }

        String registrationId = Datastore.getGCMRegistrationId(App.getApp());

        if (TextUtils.isEmpty(registrationId)) {
            Intent intent = new Intent(this, GcmRegistrationIntentService.class);
            startService(intent);
        }

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        IntentFilter filter = new IntentFilter();
        filter.addAction("GCM-Notification");
        filter.addAction("ReloadingView");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);

        //new AsyncGetAllMessage(true).execute();

    }

    @Override
    protected void onStart() {
        super.onStart();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(101);
        appOnOff = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        appOnOff = false;
    }

    private String getUnReadName() {
        return getString(R.string.unread) + " (" + getUnReadMessageHolder().size() + ")";
    }

    private String getReadName() {
        return getString(R.string.read) + " (" + getReadMessageHolder().size() + ")";
    }

    private String getMarkName() {
        return getString(R.string.mark) + " (" + getMarkMessageHolder().size() + ")";
    }

    private List<AdminMessageHolder> getUnReadMessageHolder() {
        List<AdminMessageHolder> unReadMessages = new ArrayList<>();
        if (messageHolderList != null) {
            for (AdminMessageHolder messageHolder : messageHolderList) {
                if (messageHolder.getMessageDetails().getSeen() == null) {
                    if (messageHolder.getMessageDetails().getFromid() != 1) {
                        unReadMessages.add(messageHolder);
                    }
                }
            }
        }
        return unReadMessages;
    }

    private List<AdminMessageHolder> getReadMessageHolder() {
        List<AdminMessageHolder> readMessages = new ArrayList<>();
        if (messageHolderList != null) {
            for (AdminMessageHolder messageHolder : messageHolderList) {
                if (messageHolder.getMessageDetails().getFromid() != 1) {
                    if (messageHolder.getMessageDetails().getSeen() != null) {
                        readMessages.add(messageHolder);
                    }
                } else {
                    readMessages.add(messageHolder);
                }

            }
        }
        return readMessages;
    }

    private List<AdminMessageHolder> getMarkMessageHolder() {
        List<AdminMessageHolder> readMessages = new ArrayList<>();
        if (messageHolderList != null) {
            for (AdminMessageHolder messageHolder : messageHolderList) {
                if (messageHolder.getMessageDetails().getMarkstatus() == 1) {
                    readMessages.add(messageHolder);
                }

            }
        }
        return readMessages;
    }

    public void broadcastPageCall() {
        startActivity(new Intent(this, BroadCastActivity.class));
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {

            super.onBackPressed();
            finish();
        }

        if (getFragmentManager().getBackStackEntryCount() > 0) {

            getFragmentManager().popBackStack();

        } else {
            this.doubleBackToExitPressedOnce = true;
            UiUtils.showToast(getResources().getString(R.string.exit_message));
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }

    @Override
    public void finish() {
        isReload = true;
        super.finish();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.broadcast:
                broadcastPageCall();
                return true;
            case R.id.sync:
                new AsyncGetAllMessage(true).execute();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void progressdialog() {
        progressBar = new ProgressDialog(HomeActivity.this);
        progressBar.setCancelable(false);
        progressBar.setMessage(getResources().getString(R.string.loading));
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
    }

    private void loadData() {
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentTab = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setCurrentItem(currentTab);
    }


    @Override
    protected void onDestroy() {
        try {
            if (mMessageReceiver != null){
                LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
            }
            super.onDestroy();
        } catch (NullPointerException npe) {
            Log.e("NPE:", " Bug workaround");
        }
    }

    public static void setReload(boolean reload) {
        isReload = reload;
    }

    public static int getCurrentTab() {
        return currentTab;
    }

    public interface Updateable {
        public void update(List<AdminMessageHolder> messageHolders);
    }

    class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            Gson gson = new Gson();

            switch (position) {
                case 0:
                    UnReadFragment fragment = new UnReadFragment();
                    String unReadMessage = "";
                    if (messageHolderList.size() > 0) {
                        unReadMessage = gson.toJson(messageHolderList);
                    }

                    bundle.putString("data", unReadMessage);
                    fragment.setArguments(bundle);
                    return fragment;
                case 1:
                    ReadFragment fragment1 = new ReadFragment();
                    String readMessage = "";
                    if (messageHolderList.size() > 0) {
                        readMessage = gson.toJson(messageHolderList);
                    }

                    bundle.putString("data", readMessage);
                    fragment1.setArguments(bundle);
                    return fragment1;

                case 2:
                    MarkFragment markFragment = new MarkFragment();
                    return markFragment;
            }
            return null;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getUnReadName();
                case 1:
                    return getReadName();
                case 2:
                    return getMarkName();
            }
            return null;
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            try{
                super.finishUpdate(container);
            } catch (NullPointerException nullPointerException){
                Log.e("finishUpdate","Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
            }
        }
    }

    private class AsyncGetAllMessage extends AsyncTask<Void, Void, Integer> {

        private final boolean dialogvisibility;
        ArrayList<AdminMessageHolder> mobject;
        int count = 0;


        public AsyncGetAllMessage(boolean visibility) {
            dialogvisibility = visibility;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialogvisibility) {
                progressdialog();
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.GET_ALL_MESSAGE;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed && ++count < 5) {
                try {
                    mobject = ApiUtils.downloadDashboardDetails(url, 1000);

                    success = (mobject != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {
                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }
            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;
        }


        @Override
        protected void onPostExecute(Integer result) {
            progressBar.dismiss();
            messageHolderList.clear();

            if (LOGIN_FAILED == result) {
                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return;
            } else if (SUCCESS == result) {
                if (mobject != null) {
                    if (mobject.size() > 0) {
                        messageHolderList = mobject;
                        loadData();
                    }
                }
            } else if (FAILED == result) {
                if (messageHolderList.size() > 0) {
                    messageHolderList.clear();
                }

                loadData();
                UiUtils.showSnackBare(parLinearLayout, getResources().getString(R.string.servernotreachable));

            } else if (INTERNET_DISCONNECTED == result) {
                loadData();
            }
        }
    }
}
