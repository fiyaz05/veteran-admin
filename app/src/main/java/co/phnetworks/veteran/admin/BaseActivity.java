package co.phnetworks.veteran.admin;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


/**
 * Created by mohamed on 24/2/16.
 */
public class BaseActivity extends AppCompatActivity {

    ActionBar actionBar;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void setupToolBar(boolean visible) {

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();
        mToolbar.setLogo(R.drawable.actionbar_space_between_icon_and_title);
        mToolbar.setTitleTextColor(getResources().getColor(R.color.cardview_light_background));
        mToolbar.setSubtitleTextColor(getResources().getColor(R.color.cardview_light_background));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

                if (getFragmentManager().getBackStackEntryCount() > 0) {

                }else {

                }

            }
        });

        mToolbar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_spinner);

        if (mProgressBar != null) {
            mProgressBar.getIndeterminateDrawable().setColorFilter(Color.WHITE, android.graphics.PorterDuff.Mode.SRC_IN);
        }

    }

    protected void setSubtitle(String subtitle){
        mToolbar.setSubtitle(subtitle);
    }

    protected void navigationdisplay(boolean visible){

          if (actionBar != null) {
              actionBar.setHomeButtonEnabled(visible);
              actionBar.setDisplayHomeAsUpEnabled(visible);
              Drawable backArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_18dp);
              backArrow.setColorFilter(getResources().getColor(R.color.cardview_light_background), PorterDuff.Mode.SRC_ATOP);
              getSupportActionBar().setHomeAsUpIndicator(backArrow);
          }
    }

    protected void setProgress(boolean visible) {
        mProgressBar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
}
