package co.phnetworks.veteran.admin.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import co.phnetworks.veteran.admin.App;
import co.phnetworks.veteran.admin.R;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;

/**
 * Created by mohamed on 19/4/16.
 */
public class UiUtils {

    /**
     * Returns time stamp in an appropriate format
     * @param date date
     * @return String
     */
    public static String getTimeStampString(long date) {

        if (DateUtils.isToday(date)) {
            return DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date(date));
        }

        DateTime createdDateTime = new DateTime(date).plusDays(1);
        if (DateUtils.isToday(createdDateTime.getMillis())) {
            return App.getApp().getResources().getString(R.string.yesterday);
        }

        return UiUtils.getDateString(date);
    }

    /**
     * Returns time stamp in an appropriate format
     * @param time Time
     * @return Time string
     */
    public static String getDateString(long time) {
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd/MM/yy");
        return fmtOut.format(time);

       /* DateFormat df = android.text.format.DateFormat.getDateFormat(App.getApp());
        return df.format(new Date(time));*/
    }

    public static void showToast(String message){
        Toast.makeText(App.getApp(),
                message, Toast.LENGTH_SHORT)
                .show();
    }

    public static void showSnackBare(View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    /**
     * Hides keyboard
     * @param context Context
     * @param editText Edit Text
     */
    public static void hideKeyboard(Context context, EditText editText) {

        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

    }

    public static void hideSoftKeyboard(Context context, View view){
        InputMethodManager imm =(InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public static void profileWidget(AdminMessageHolder adminMessageHolder, Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        LayoutInflater inflater = (LayoutInflater) App.getApp().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout_view = inflater.inflate(R.layout.profile_details, null);
        builder.setView(layout_view);
        //builder.setCancelable(false);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        CircleImageView circleImageView = (CircleImageView) layout_view.findViewById(R.id.profile_picture);
        TextView name = (TextView) layout_view.findViewById(R.id.name_field);
        TextView rank = (TextView) layout_view.findViewById(R.id.rank);
        TextView phonenumber = (TextView) layout_view.findViewById(R.id.phone_number_field);
        TextView regcorp = (TextView) layout_view.findViewById(R.id.regtcorps);
        TextView type = (TextView) layout_view.findViewById(R.id.type);
        TextView emailaddress = (TextView) layout_view.findViewById(R.id.email);
        String url = App.BASE_URL+App.GET_PROFILE_PICTURE+adminMessageHolder.getVeteranDetail().getId();

        name.setText(adminMessageHolder.getVeteranDetail().getName());
        phonenumber.setText(adminMessageHolder.getVeteranDetail().getPhonenumber());
        emailaddress.setText(adminMessageHolder.getVeteranDetail().getEmailaddress());
        rank.setText(Datastore.rank[adminMessageHolder.getVeteranDetail().getRank()]);
        Picasso picasso = App.getApp().getPicasso();
        picasso.load(url)
                .centerInside()
                .fit()
                .config(Bitmap.Config.ARGB_8888)
                .placeholder(R.drawable.ic_contact_picture)
                .error(R.drawable.ic_contact_picture)
                .into(circleImageView);

        if (adminMessageHolder.getVeteranDetail().getRegtcorps() == 1) {
            regcorp.setText(App.getApp().getResources().getString(R.string.vet_corps));
            type.setText(Datastore.typecat[adminMessageHolder.getVeteranDetail().getType()]);
        } else {
            regcorp.setText(App.getApp().getResources().getString(R.string.vet_regi));
            type.setText(Datastore.regiment[adminMessageHolder.getVeteranDetail().getType()]);
        }


    }
}
