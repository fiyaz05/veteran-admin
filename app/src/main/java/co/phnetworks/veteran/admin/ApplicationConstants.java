package co.phnetworks.veteran.admin;

/**
 * Created by Seenivasan on 28/3/16.
 */
public final class ApplicationConstants {

    //Client details
    public static final int PLATFORM_ANDROID = 1;

    //Client details
    public static final int CLIENT_TYPE = 1;

    //Admin details
    public static final int ADMIN_TYPE = 0;

    public static final int MARKED = 1;

    public static final int UNMARKED = 0;


}
