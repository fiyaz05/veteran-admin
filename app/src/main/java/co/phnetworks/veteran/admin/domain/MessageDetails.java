package co.phnetworks.veteran.admin.domain;

/**
 * Created by mohamed on 29/3/16.
 */
public class MessageDetails {
    private Long id;
    private Long fromid;
    private Long toid;
    private int messagesentby;
    private int broadcast;
    private Long created;
    private Long delivered;
    private Long seen;
    private String message;
    private Long latestupdate;
    private int markstatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromid() {
        return fromid;
    }

    public void setFromid(Long fromid) {
        this.fromid = fromid;
    }

    public Long getToid() {
        return toid;
    }

    public void setToid(Long toid) {
        this.toid = toid;
    }

    public int getMessagesentby() {
        return messagesentby;
    }

    public void setMessagesentby(int messagesentby) {
        this.messagesentby = messagesentby;
    }

    public int getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(int broadcast) {
        this.broadcast = broadcast;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public Long getDelivered() {
        return delivered;
    }

    public void setDelivered(Long delivered) {
        this.delivered = delivered;
    }

    public Long getSeen() {
        return seen;
    }

    public void setSeen(Long seen) {
        this.seen = seen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getLatestupdate() {
        return latestupdate;
    }

    public void setLatestupdate(Long latestupdate) {
        this.latestupdate = latestupdate;
    }

    @Override
    public String toString() {
        return "MessageDetails{" +
                "id=" + id +
                ", fromid=" + fromid +
                ", toid=" + toid +
                ", messagesentby=" + messagesentby +
                ", broadcast=" + broadcast +
                ", created=" + created +
                ", delivered=" + delivered +
                ", seen=" + seen +
                ", message='" + message + '\'' +
                ", latestupdate=" + latestupdate +
                '}';
    }

    public int getMarkstatus() {
        return markstatus;
    }

    public void setMarkstatus(int markstatus) {
        this.markstatus = markstatus;
    }
}
