/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.phnetworks.veteran.admin.common.gcm.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import co.phnetworks.veteran.admin.App;
import co.phnetworks.veteran.admin.ApplicationConstants;
import co.phnetworks.veteran.admin.R;
import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.Installation;
import co.phnetworks.veteran.admin.domain.ApplicationInstallation;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;

public class GcmRegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public GcmRegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);


            //Implement this method to send any registration to your app's servers.
            boolean success = sendRegistrationToServer(token);

            if (success) {
                Datastore.storeGCMRegistrationId(App.getApp(), token);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Modify this method to associate the user's GCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private boolean sendRegistrationToServer(String token) throws SessionExpiredException, NotSignedInException {
        if (TextUtils.isEmpty(token)) {
            return false;
        }
        try {

            ApplicationInstallation installation = new ApplicationInstallation();
            Gson gson = new Gson();

            installation.setClienttype(ApplicationConstants.CLIENT_TYPE);
            installation.setDeviceName(android.os.Build.MODEL);
            installation.setClientplatform(ApplicationConstants.PLATFORM_ANDROID);
            installation.setAppversion(GenericUtils.getAppVersion(App.getApp()));
            installation.setIdentifier(Installation.id(App.getApp()));
            installation.setPushmessagingid(token);
            installation.setPushmessagingidgenerated(System.currentTimeMillis());

            ApiUtils.GcmRegisteration(installation);
            return true;

        } catch ( JsonSyntaxException | IOException e ) {
            return false;
        }
    }
}
