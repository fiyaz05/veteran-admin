package co.phnetworks.veteran.admin.domain;

import java.io.Serializable;

/**
 * Created by Seenivasan on 28/3/16.
 */
public class ApplicationInstallation implements Serializable {

    private long id;
    private long ownerid;
    private int clienttype;
    private String deviceName;
    private int clientplatform;
    private int appversion;
    private String identifier;
    private String pushmessagingid;
    private long created;
    private long pushmessagingidgenerated;
    private long lastmodified;
    private long deleted;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(long ownerid) {
        this.ownerid = ownerid;
    }

    public int getClienttype() {
        return clienttype;
    }

    public void setClienttype(int clienttype) {
        this.clienttype = clienttype;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public int getClientplatform() {
        return clientplatform;
    }

    public void setClientplatform(int clientplatform) {
        this.clientplatform = clientplatform;
    }

    public int getAppversion() {
        return appversion;
    }

    public void setAppversion(int appversion) {
        this.appversion = appversion;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getPushmessagingid() {
        return pushmessagingid;
    }

    public void setPushmessagingid(String pushmessagingid) {
        this.pushmessagingid = pushmessagingid;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getPushmessagingidgenerated() {
        return pushmessagingidgenerated;
    }

    public void setPushmessagingidgenerated(long pushmessagingidgenerated) {
        this.pushmessagingidgenerated = pushmessagingidgenerated;
    }

    public long getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(long lastmodified) {
        this.lastmodified = lastmodified;
    }

    public long getDeleted() {
        return deleted;
    }

    public void setDeleted(long deleted) {
        this.deleted = deleted;
    }
}
