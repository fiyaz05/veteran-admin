package co.phnetworks.veteran.admin;

import android.app.FragmentManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.common.gcm.service.GcmRegistrationIntentService;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import co.phnetworks.veteran.admin.exception.NotSignedInException;
import co.phnetworks.veteran.admin.exception.SessionExpiredException;

/**
 * Created by mohamed on 25/2/16.
 */

public class LandingActivity extends BaseActivity {


    private static final int SUCCESS = 1;
    private static final int INTERNET_DISCONNECTED = 2;
    private static final int LOGIN_FAILED = 3;
    private static final int FAILED = 4;
    private static final int NOQUERIES = 5;
    public static boolean appOnOff = false;
    private AnswerQueryFragment answerQueryFragment;
    private RecyclerView recyclerView;
    private ReadListAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private RelativeLayout relativeMainLayout, emptyPageLayout, noInternetLayout, parentlayout;
    private Button retryButton;
    private TextView emptymessage;
    private AdminMessageHolder adminMessageHolderobject;
    private MenuItem broadcastitem;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean titleFlag = false;
    private boolean cardclickFlag = false;
    private ProgressDialog progressBar;
    private int cardPosition;
    private Long veteranid;
    private ArrayList<AdminMessageHolder> adminMessageHolderArrayList = new ArrayList<>();
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String action = intent.getAction();

            if (action.equals("GCM-Notification")) {
                runAsyntaskForLoadingCard(false);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        relativeMainLayout = (RelativeLayout) findViewById(R.id.mainlayout);
        emptyPageLayout = (RelativeLayout) findViewById(R.id.emptypagemessage);
        noInternetLayout = (RelativeLayout) findViewById(R.id.internetconnection);
        parentlayout = (RelativeLayout) findViewById(R.id.parentlayout);
        retryButton = (Button) findViewById(R.id.nointernetbutton);
        emptymessage = (TextView) findViewById(R.id.emptymessage);

        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runAsyntaskForLoadingCard(true);
            }
        });

        String registrationId = Datastore.getGCMRegistrationId(App.getApp());

        if (TextUtils.isEmpty(registrationId)) {
            Intent intent = new Intent(this, GcmRegistrationIntentService.class);
            startService(intent);
        }

        progressBar = new ProgressDialog(LandingActivity.this);
        setupToolBar(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        mySwipeRefreshLayout.setRefreshing(true);
                        runAsyntaskForLoadingCard(false);
                    }
                }
        );
        fragmentBackstackHandling();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("GCM-Notification"));

        runAsyntaskForLoadingCard(true);
    }



    @Override
    protected void onStart() {
        super.onStart();
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(101);
        appOnOff = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        appOnOff = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void runAsyntaskForLoadingCard(boolean dialogvisibility) {
        if (GenericUtils.isInternetAvailable(LandingActivity.this)) {
            noInternetLayout.setVisibility(View.GONE);
            emptyPageLayout.setVisibility(View.GONE);
            relativeMainLayout.setVisibility(View.VISIBLE);
            final AsyncGetAllMessage asyncGetAllMessage = new AsyncGetAllMessage(dialogvisibility);
            asyncGetAllMessage.execute();

        } else {
            noInternetLayout.setVisibility(View.VISIBLE);
            emptyPageLayout.setVisibility(View.GONE);
            relativeMainLayout.setVisibility(View.GONE);
            UiUtils.showToast(getResources().getString(R.string.no_internet));
            mySwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void setupUI(ArrayList<AdminMessageHolder> recycleViewlistOject, int serverMessage) {

        if (adapter == null) {
            adapter = new ReadListAdapter(this, recycleViewlistOject);
            recyclerView.setAdapter(adapter);
        } else if (cardclickFlag && serverMessage == SUCCESS) {
            adminMessageHolderArrayList = recycleViewlistOject;
            setupParticularCard();
            cardclickFlag = false;
        } else if (serverMessage == FAILED) {
            adapter.setDataset(recycleViewlistOject);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        } else {
            adapter.setDataset(recycleViewlistOject);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        /*else if( adminMessageHolderArrayList.size() != recycleViewlistOject.size()){
            Log.e("notequal", "inside");

        } */

        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(this, LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
        adapter.SetOnItemClickListener(new ReadListAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(View v, AdminMessageHolder adminMessageHolder, int position) {


                cardPosition = position;
                cardclickFlag = true;
                titleFlag = true;
                adminMessageHolderobject = adminMessageHolder;
                veteranid = adminMessageHolder.getVeteranDetail().getId();
                answerQueryFragment = new AnswerQueryFragment(veteranid);

                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_holder, answerQueryFragment, "answer_query_fragment")
                        .addToBackStack("answerquery")
                        .commit();
            }
        });

        adapter.SetOnProfileClickListener(new ReadListAdapter.OnProfileClickListener() {
            @Override
            public void oncontactpicClick(View view, AdminMessageHolder adminMessageHolder) {
                UiUtils.profileWidget(adminMessageHolder, LandingActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        hideSoftKeyboard(parentlayout);
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        if (getFragmentManager().getBackStackEntryCount() > 0) {

            getFragmentManager().popBackStack();

        } else {
            this.doubleBackToExitPressedOnce = true;
            navigationdisplay(false);
            UiUtils.showToast(getResources().getString(R.string.exit_message));
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }

    private void fragmentBackstackHandling() {
        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (getFragmentManager().getBackStackEntryCount() > 0) {

                    navigationdisplay(true);
                    broadcastitem.setVisible(false);
                    if (titleFlag) {
                        titleFlag = false;
                        // setTitle(Datastore.rank[adminMessageHolderobject.getVeteranDetail().getRank()]);
                        setTitle(adminMessageHolderobject.getVeteranDetail().getName());
                    } else {
                        setTitle(getResources().getString(R.string.broadcast));
                    }

                } else {
                    navigationdisplay(false);
                    broadcastitem.setVisible(true);
                    setTitle(getResources().getString(R.string.app_name));
                    setSubtitle("");
                    runAsyntaskForLoadingCard(false);
                }
            }
        });
    }

    public void progressdialog() {
        progressBar.setCancelable(false);
        progressBar.setMessage(getResources().getString(R.string.loading));
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
    }

    private void setupParticularCard() {

        AdminMessageHolder adminMessageHolder = new AdminMessageHolder();
        if (veteranid != null) {
            for (int i = 0; i < adminMessageHolderArrayList.size(); i++) {
                if (adminMessageHolderArrayList.get(i).getVeteranDetail().getId() == veteranid) {
                    adminMessageHolder = adminMessageHolderArrayList.get(i);
                }
            }

            adapter.setParticularDataset(adminMessageHolder, veteranid);
            adapter.notifyItemChanged(cardPosition);
        }
    }

    public void broadcastPageCall() {
        answerQueryFragment = new AnswerQueryFragment();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_holder, answerQueryFragment, "answer_query_fragment")
                .addToBackStack("answerquery")
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        broadcastitem = menu.findItem(R.id.broadcast);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.broadcast:
                broadcastPageCall();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class AsyncGetAllMessage extends AsyncTask<Void, Void, Integer> {

        ArrayList<AdminMessageHolder> mobject;
        boolean dialogvisibility;
        int count = 0;

        public AsyncGetAllMessage(boolean visibility) {
            dialogvisibility = visibility;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (dialogvisibility) {
                progressdialog();
            }

        }

        @Override
        protected Integer doInBackground(Void... params) {

            String url = App.BASE_URL + App.GET_ALL_MESSAGE;

            if (!GenericUtils.isInternetAvailable(App.getApp())) {
                return INTERNET_DISCONNECTED;
            }

            boolean loginFailed = false, success = false;
            while (!loginFailed && ++count < 5) {
                try {
                    mobject = ApiUtils.downloadDashboardDetails(url, 1000);
                    success = (mobject != null);
                    break;
                } catch (SessionExpiredException e) {
                    String username = Datastore.getUsername();
                    String password = Datastore.getPassword();

                    if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                        loginFailed = true;
                        break;
                    }

                    try {
                        loginFailed = !ApiUtils.login(App.getApp().getOk(), username, password);
                    } catch (ForbiddenException e2) {

                    } catch (InvalidInputException e3) {

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }

                } catch (NotSignedInException e) {
                    loginFailed = true;
                } catch (IOException e) {
                    return FAILED;
                }
            }

            if (loginFailed) {
                Datastore.clear();
            }
            return loginFailed ? LOGIN_FAILED : success ? SUCCESS : FAILED;
        }


        @Override
        protected void onPostExecute(Integer result) {

            progressBar.dismiss();
            if (LOGIN_FAILED == result) {
                mySwipeRefreshLayout.setRefreshing(false);
                Intent intent = new Intent(LandingActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                return;
            }

            if (SUCCESS == result) {

                if (mobject != null) {

                    mySwipeRefreshLayout.setRefreshing(false);
                    if (mobject.size() > 0) {
                        emptyPageLayout.setVisibility(View.GONE);
                        setupUI(mobject, SUCCESS);
                        adminMessageHolderArrayList = mobject;
                    } else {
                        emptyPageLayout.setVisibility(View.VISIBLE);
                        setupUI(mobject, NOQUERIES);
                    }
                }
            }

            if (FAILED == result) {
                mySwipeRefreshLayout.setRefreshing(false);
                //GenericUtils.showSimpleDialogMessage(getResources().getString(R.string.servernotreachable),LandingActivity.this);
                emptymessage.setText(getResources().getString(R.string.servernotreachable));
                emptyPageLayout.setVisibility(View.VISIBLE);
                if (adminMessageHolderArrayList.size() > 0) {
                    adminMessageHolderArrayList.clear();
                }
                setupUI(adminMessageHolderArrayList, FAILED);
            }
        }
    }

}
