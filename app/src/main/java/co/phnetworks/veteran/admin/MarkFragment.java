package co.phnetworks.veteran.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;

/**
 * Created by fiyaz on 1/3/17.
 */

public class MarkFragment extends Fragment {


    private RecyclerView recyclerView;
    private MarkListAdapter adapter;
    private TextView no_internet, message;
    private List<AdminMessageHolder> readMessages = new ArrayList<>();
    private HomeActivity homeActivity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_mark, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.mark_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        no_internet = (TextView) view.findViewById(R.id.no_internet);
        message = (TextView) view.findViewById(R.id.message);

        populateData();
    }

    private void populateData() {
        readMessages.clear();
        homeActivity = (HomeActivity) getActivity();
        if (homeActivity.getMessageHolderList() != null) {
            for (AdminMessageHolder messageHolder : homeActivity.getMessageHolderList()) {
                if (messageHolder.getMessageDetails().getMarkstatus() == 1) {
                    readMessages.add(messageHolder);
                }
            }
        }
        loadUI();
    }

    private void loadUI() {

        adapter = new MarkListAdapter(getContext(), readMessages);

        if (!GenericUtils.isInternetAvailable(getActivity())) {
            readMessages.clear();
            no_internet.setVisibility(View.VISIBLE);

        } else if (readMessages != null) {
            no_internet.setVisibility(View.GONE);
            message.setVisibility(View.GONE);
            adapter.setDataset(readMessages);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            if (readMessages.size() == 0) {
                message.setText(getResources().getString(R.string.no_result_found));
                message.setVisibility(View.VISIBLE);
            }

            adapter.SetOnItemClickListener(new MarkListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View v, AdminMessageHolder adminMessageHolder, int position) {
                    homeActivity.setReload(false);
                    Intent intent = new Intent(getActivity(), VeteranMessageActivity.class);
                    Gson gson = new Gson();
                    intent.putExtra("veteran", gson.toJson(adminMessageHolder.getVeteranDetail()));
                    startActivity(intent);
                }
            });

            adapter.SetOnProfileClickListener(new MarkListAdapter.OnProfileClickListener() {
                @Override
                public void oncontactpicClick(View view, AdminMessageHolder adminMessageHolder) {
                    UiUtils.profileWidget(adminMessageHolder, getActivity());
                }
            });
            adapter.notifyDataSetChanged();
        } else {
            message.setText(getResources().getString(R.string.no_result_found));
            message.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }

    }
}
