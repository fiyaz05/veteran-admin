package co.phnetworks.veteran.admin;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;

import co.phnetworks.veteran.admin.Utils.ApiUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;
import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;

/**
 * Created by Seenivasan on 5/4/16.
 */
public class LoginActivity extends BaseActivity implements TextWatcher{

    private EditText userNameText;
    private EditText passwordText;
    private RelativeLayout parentloginlayout;
    private Button loginButton;
    private String userName;
    private String password;
    private ProgressDialog dialog = null;
    private TextInputLayout usernamelayout,passwordlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        parentloginlayout = (RelativeLayout) findViewById(R.id.login_parent);
        userNameText = (EditText) findViewById(R.id.username);
        passwordText = (EditText) findViewById(R.id.password);
        loginButton = (Button) findViewById(R.id.btn_login);
        usernamelayout = (TextInputLayout) findViewById(R.id.inputuserlayout);
        passwordlayout = (TextInputLayout) findViewById(R.id.inputpasswordlayout);

        userNameText.addTextChangedListener(this);
        passwordText.addTextChangedListener(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginAccount();
            }
        });


    }

    public void hidekeyboardOnclickoutside(View view){
        UiUtils.hideSoftKeyboard(this,parentloginlayout);
    }

    public void loginAccount() {
        userName = userNameText.getText().toString();
        password = passwordText.getText().toString();

        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(password)) {

            if (GenericUtils.isInternetAvailable(LoginActivity.this)) {

                final AsyncLoginTask asyncLoginTask = new AsyncLoginTask();
                asyncLoginTask.execute();

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (asyncLoginTask.getStatus() == AsyncTask.Status.RUNNING) {
                            asyncLoginTask.cancel(true);
                            dialog.cancel();
                            visibleAndInvisibleComponentsOnPostExecute();
                            UiUtils.showToast(getResources().getString(R.string.slow_connection));
                        }
                    }
                }, 100000);
            }else {
                UiUtils.showToast(getResources().getString(R.string.no_internet));
            }
        } else {
            if (userName.isEmpty()) {
                usernamelayout.setError(getResources().getString(R.string.user_error_message));
            }
            if (password.isEmpty()) {
                passwordlayout.setError(getResources().getString(R.string.password_error_message));
            }
            onLoginFailed();
        }
    }

    private boolean isSuccess(String response) {
        if (response.equals(this.getString(R.string.UNAUTHORIZED_RESPONSE))) {
            return false;
        } else if (response.equals(this.getString(R.string.RESPONSE_OK))) {
            return true;
        } else if (response.equals(userName)) {
           return true;
        }
        return false;
    }

    public void onLoginSuccess() {

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(this, getResources().getString(R.string.login_failed), Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    private void showLoginFailedMessage(String response) {
        visibleAndInvisibleComponentsOnPostExecute();
        AlertDialog.Builder failureMessageBuilder = new AlertDialog.Builder(this);
        failureMessageBuilder.setTitle(getString(R.string.authenticationFailed));
        if (response.equals("403")) {
            failureMessageBuilder.setMessage(getString(R.string.restricted));
        }else if (response.equals("406")) {
            failureMessageBuilder.setMessage(getString(R.string.unauthoristed_user));
        } else {
            failureMessageBuilder.setMessage(getString(R.string.authenticationFailed_message));
        }
        failureMessageBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = failureMessageBuilder.create();
        alertDialog.show();
    }

    private void visibleAndInvisibleComponentsOnPostExecute() {
        userNameText.setVisibility(View.VISIBLE);
        passwordText.setVisibility(View.VISIBLE);
        loginButton.setVisibility(View.VISIBLE);
    }

    private void visibleAndInvisibleComponentsOnPreExecute() {
        userNameText.setVisibility(View.INVISIBLE);
        passwordText.setVisibility(View.INVISIBLE);
        loginButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (userNameText.getText().length() > 0) {
            usernamelayout.setError(null);
        }
        if (passwordText.getText().length() > 0) {
            passwordlayout.setError(null);
        }

    }

    private class AsyncLoginTask extends AsyncTask<String, String, String> {
        boolean status = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(LoginActivity.this);
            visibleAndInvisibleComponentsOnPreExecute();
            dialog.setMessage(getResources().getString(R.string.authenticating));
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            try {
                status = ApiUtils.login(App.getApp().getOk(), userName,password);
            } catch (ForbiddenException e){
                response =  String.valueOf(HttpURLConnection.HTTP_FORBIDDEN);
            } catch (InvalidInputException e){
                response = String.valueOf(HttpURLConnection.HTTP_NOT_ACCEPTABLE);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.e("response", String.valueOf(status));
            if (status) {
                onLoginSuccess();
                Datastore.saveUserName(userName);
                Datastore.savePassword(password);
            } else {

                if(response.equals(String.valueOf(HttpURLConnection.HTTP_FORBIDDEN))){
                    showLoginFailedMessage("403");
                }else if (response.equals(String.valueOf(HttpURLConnection.HTTP_NOT_ACCEPTABLE))) {
                    showLoginFailedMessage("406");
                }else {
                    showLoginFailedMessage("401");
                }
            }
            dialog.dismiss();
        }
    }
}
