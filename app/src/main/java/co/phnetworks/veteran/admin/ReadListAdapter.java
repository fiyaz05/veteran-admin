package co.phnetworks.veteran.admin;

/**
 * Created by mohamed on 24/2/16.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import co.phnetworks.veteran.admin.Utils.CircleImageView;
import co.phnetworks.veteran.admin.Utils.UiConstant;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;


public class ReadListAdapter extends RecyclerView.Adapter<ReadListAdapter.ViewHolder> {

    private static List<AdminMessageHolder> dataset;
    private Context mcontext;
    private OnItemClickListener mItemClickListener;
    private OnProfileClickListener onProfileClickListener;

    public ReadListAdapter(Context context, List<AdminMessageHolder> mDataset) {
        this.mcontext = context;
        this.dataset = mDataset;
    }

    public void setDataset(List<AdminMessageHolder> data){
        dataset = data;
    }
    public void setParticularDataset(AdminMessageHolder data, Long veteranid){

        for (int i=0;i<dataset.size();i++){
            if(dataset.get(i).getVeteranDetail().getId() == veteranid){
                dataset.get(i).setMessageDetails(data.getMessageDetails());
                dataset.get(i).setVeteranDetail(data.getVeteranDetail());
            }
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycleview_itemview, parent, false);
        ViewHolder viewHolder = new ViewHolder(view, mItemClickListener, onProfileClickListener);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (dataset.get(position).getVeteranDetail() != null) {

            String url = App.BASE_URL+App.GET_PROFILE_PICTURE+dataset.get(position).getVeteranDetail().getId();

            Picasso picasso = App.getApp().getPicasso();
            picasso.load(url)
                   .centerInside()
                   .fit()
                   .config(Bitmap.Config.ARGB_8888)
                   .placeholder(R.drawable.ic_contact_picture)
                   .error(R.drawable.ic_contact_picture)
                   .into(holder.circleImageView);

            // holder.textView_rank.setText(Datastore.rank[dataset.get(position).getVeteranDetail().getRank()]);
            holder.textView_name.setText(dataset.get(position).getVeteranDetail().getName().trim());
            holder.textView_lastmessage.setText(dataset.get(position).getMessageDetails().getMessage().trim());
            holder.textView_time.setText(UiUtils.getTimeStampString(dataset.get(position).getMessageDetails().getCreated()));


            if(dataset.get(position).getMessageDetails().getMessagesentby() == UiConstant.MESSAGE_SENT_BY_ADMIN){
                holder.textView_lastmessage.setTypeface(Typeface.DEFAULT);
                holder.donetick.setVisibility(View.VISIBLE);
                if (dataset.get(position).getMessageDetails().getSeen() != null) {
                    holder.donetick.setImageResource(R.drawable.doubletick);
                    holder.donetick.setColorFilter(App.getApp().getResources().getColor(R.color.sky_blue));
                }  else if (dataset.get(position).getMessageDetails().getDelivered() != null) {
                    holder.donetick.setImageResource(R.drawable.doubletick);
                    holder.donetick.setColorFilter(App.getApp().getResources().getColor(R.color.divider));
                } else {
                    holder.donetick.setImageResource(R.drawable.singletick);
                    holder.donetick.setColorFilter(App.getApp().getResources().getColor(R.color.divider));
                }

            } else {
                holder.donetick.setVisibility(View.GONE);

                if(dataset.get(position).getMessageDetails().getSeen() == null){
                    holder.textView_lastmessage.setTypeface(Typeface.DEFAULT_BOLD);
                }else {
                    holder.textView_lastmessage.setTypeface(Typeface.DEFAULT);
                }
            }


        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;

    }

    public void SetOnProfileClickListener(final OnProfileClickListener mItemClickListener) {
        this.onProfileClickListener = mItemClickListener;

    }

    public interface OnItemClickListener {
        void onItemClick(View view, AdminMessageHolder q, int position);
    }

    public interface OnProfileClickListener {
        void oncontactpicClick(View view, AdminMessageHolder q);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        protected OnItemClickListener mItemClickListener;
        protected OnProfileClickListener cProfilepicListener;
        protected TextView textView_rank;
        protected TextView textView_lastmessage;
        protected TextView textView_time;
        protected TextView textView_name;
        protected RelativeLayout wholeView;
        protected ImageView donetick;
        protected CircleImageView circleImageView;

        public ViewHolder(View itemView, OnItemClickListener listener, OnProfileClickListener Plistener) {
            super(itemView);
            //  textView_rank = (TextView) itemView.findViewById(R.id.list_item_rank);
            textView_name = (TextView) itemView.findViewById(R.id.list_item_name);
            textView_lastmessage = (TextView) itemView.findViewById(R.id.list_item_lastmessage);
            textView_time = (TextView) itemView.findViewById(R.id.list_item_time);
            donetick = (ImageView) itemView.findViewById(R.id.donetick);
            wholeView = (RelativeLayout) itemView.findViewById(R.id.fullview);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.contactimage);
            wholeView.setOnClickListener(this);
            circleImageView.setOnClickListener(this);
            mItemClickListener = listener;
            cProfilepicListener = Plistener;
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null && v == wholeView) {
                AdminMessageHolder adminMessageHolder = dataset.get(getAdapterPosition());
                mItemClickListener.onItemClick(v, adminMessageHolder, getAdapterPosition());
            }
            if (cProfilepicListener != null && v == circleImageView) {
                AdminMessageHolder adminMessageHolder = dataset.get(getAdapterPosition());
                cProfilepicListener.oncontactpicClick(v, adminMessageHolder);
            }
        }
    }

}
