package co.phnetworks.veteran.admin.exception;

public class NotSignedInException extends Exception {

    @Override
    public String getMessage() {
        return "Not signed in";
    }
}