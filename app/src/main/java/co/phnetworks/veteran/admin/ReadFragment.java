package co.phnetworks.veteran.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import co.phnetworks.veteran.admin.Utils.GenericUtils;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.AdminMessageHolder;


public class ReadFragment extends Fragment {

    //private OnFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private ReadListAdapter adapter;
    private TextView no_internet, message;
    private List<AdminMessageHolder> readMessages = new ArrayList<>();
    private HomeActivity homeActivity;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();

    }

    private void populateData() {
        readMessages.clear();
        homeActivity = (HomeActivity) getActivity();
        if (homeActivity.getMessageHolderList() != null) {

            for (AdminMessageHolder messageHolder : homeActivity.getMessageHolderList()) {
                if (messageHolder.getMessageDetails().getFromid() != 1) {
                    if (messageHolder.getMessageDetails().getSeen() != null) {
                        readMessages.add(messageHolder);
                    }
                } else {
                    readMessages.add(messageHolder);
                }

            }
        }
        loadUI();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_read, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.read_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        no_internet = (TextView) view.findViewById(R.id.no_internet);
        message = (TextView) view.findViewById(R.id.message);

        populateData();
    }

    private void loadUI() {

        adapter = new ReadListAdapter(getContext(), readMessages);

        if (!GenericUtils.isInternetAvailable(getActivity())) {
            readMessages.clear();
            no_internet.setVisibility(View.VISIBLE);

        } else if (readMessages != null) {
            no_internet.setVisibility(View.GONE);
            message.setVisibility(View.GONE);
            adapter.setDataset(readMessages);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            if (readMessages.size() == 0) {
                message.setText(getResources().getString(R.string.no_result_found));
                message.setVisibility(View.VISIBLE);
            }

            adapter.SetOnItemClickListener(new ReadListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View v, AdminMessageHolder adminMessageHolder, int position) {
                    homeActivity.setReload(false);
                    Intent intent = new Intent(getActivity(), VeteranMessageActivity.class);
                    Gson gson = new Gson();
                    intent.putExtra("veteran", gson.toJson(adminMessageHolder.getVeteranDetail()));
                    startActivity(intent);
                }
            });

            adapter.SetOnProfileClickListener(new ReadListAdapter.OnProfileClickListener() {
                @Override
                public void oncontactpicClick(View view, AdminMessageHolder adminMessageHolder) {
                    UiUtils.profileWidget(adminMessageHolder, getActivity());
                }
            });
            adapter.notifyDataSetChanged();
        } else {
            message.setText(getResources().getString(R.string.no_result_found));
            message.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
   /* public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }*/
}
