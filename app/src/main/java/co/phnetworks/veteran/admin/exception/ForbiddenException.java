package co.phnetworks.veteran.admin.exception;

/**
 * Created by mohamed on 4/5/16.
 */
public class ForbiddenException extends Exception{
    @Override
    public String getMessage() {
        return "Forbidden";
    }
}
