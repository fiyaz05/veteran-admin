package co.phnetworks.veteran.admin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.viewpagerindicator.PageIndicator;

import co.phnetworks.veteran.admin.Utils.ApplicationUtils;
import co.phnetworks.veteran.admin.Utils.Datastore;


public class IntroActivity extends FragmentActivity {

    private DemoFragmentAdapter mAdapter;
    private ViewPager mPager;
    private PageIndicator mIndicator;
    private RelativeLayout splashlayout,slidelayout;
    private int SPLASH_DISPLAY_LENGTH = 3000;
    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        if (!BuildConfig.DEBUG) {
            if (!ApplicationUtils.checkAppSignature(this)) {
                showLaunchFailed();
                return;
            }
        }

//        if (Build.VERSION.SDK_INT < 16) {
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        } else {
//            View decorView = getWindow().getDecorView();
//            // Hide the status bar.
//            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//            decorView.setSystemUiVisibility(uiOptions);
//        }

        setContentView(R.layout.intro_activity);
        slidelayout = (RelativeLayout) findViewById(R.id.slidelayout);
        splashlayout = (RelativeLayout) findViewById(R.id.splashlayout);

        splashlayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                splashlayout.setVisibility(View.GONE);
                Intent intent = new Intent(IntroActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

        setupUI();
    }

    private void setupUI() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new DemoFragmentAdapter(getSupportFragmentManager());

        mPager.setAdapter(mAdapter);

        mIndicator = (PageIndicator) findViewById(R.id.indicator);
        mIndicator.setViewPager(mPager);
    }

    public void redirectToIntroActivity(){
        final String username = Datastore.getUsername();
        final String password = Datastore.getPassword();
        slidelayout.setVisibility(View.GONE);
        splashlayout.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //mProgressDialog.cancel();
                if (username != null && password != null){
                    Intent intent = new Intent(IntroActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    class DemoFragmentAdapter extends FragmentStatePagerAdapter {

        Fragment[] mFragments;

        public DemoFragmentAdapter(FragmentManager fm) {
            super(fm);

            mFragments = new Fragment[] {
                    IntroFragment.getInstance(0),
                    IntroFragment.getInstance(1)
            };
        }

        @Override
        public Fragment getItem(int fragmentId) {
            return mFragments[fragmentId];
        }

        @Override
        public int getCount() {
            return 2;
        }

    }


    private void showLaunchFailed() {
        AlertDialog.Builder failureMessageBuilder = new AlertDialog.Builder(this);
        failureMessageBuilder.setTitle(getString(R.string.invalidApp));
        failureMessageBuilder.setMessage(getString(R.string.invalidApp_message));
        failureMessageBuilder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                System.exit(0);
            }
        });
        AlertDialog alertDialog = failureMessageBuilder.create();
        alertDialog.show();
    }
}
