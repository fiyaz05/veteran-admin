package co.phnetworks.veteran.admin.Utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import co.phnetworks.veteran.admin.exception.ForbiddenException;
import co.phnetworks.veteran.admin.exception.InvalidInputException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        if (SessionService.getSession() == null){

            request = request
                    .newBuilder()
                    .build();
        } else {

            request = request
                    .newBuilder()
                    .header("Cookie", SessionService.getSession())

                    .build();
        }

        Response response = chain.proceed(request);

        // Unauthorized
        if (response.code() == 403) {

            // Get new token
            OkHttpClient okclient = new OkHttpClient.Builder()
                    .build();

            boolean success = false;
            try {
                success = ApiUtils.login(okclient, Datastore.getUsername(), Datastore.getPassword());
                if (success) {
                    request = request
                            .newBuilder()
                            .removeHeader("Cookie")
                            .header("Cookie", SessionService.getSession())
                            .build();

                    response = chain.proceed(request);
                }
            } catch (ForbiddenException e) {
                e.printStackTrace();
            } catch (InvalidInputException e) {
                e.printStackTrace();
            }
        }

        return response;
    }
}
