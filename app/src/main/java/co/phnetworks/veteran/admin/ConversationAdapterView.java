package co.phnetworks.veteran.admin;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import co.phnetworks.veteran.admin.Utils.ChatLayout;
import co.phnetworks.veteran.admin.Utils.UiConstant;
import co.phnetworks.veteran.admin.Utils.UiUtils;
import co.phnetworks.veteran.admin.domain.MessageDetails;

/**
 * Created by mohamed on 11/4/16.
 */

public class ConversationAdapterView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static ArrayList<MessageDetails> dataset;
    private Context mcontext;
    private OnItemClickListener mItemClickListener;
    private boolean prevMsgIsAdmin = false;
    private boolean prevMsgIsUser = false;

    public ConversationAdapterView(Context context, ArrayList<MessageDetails> mDataset) {
        this.mcontext = context;
        this.dataset = mDataset;
    }

    public void setDataset(ArrayList<MessageDetails> data){
        dataset = data;
    }

    @Override
    public int getItemViewType(int position) {
        int viewType;
        if (dataset.get(position).getMessagesentby() == UiConstant.MESSAGE_SENT_BY_VETERAN) {
            viewType = 1;
        } else {
            viewType = 2;
        }
        return viewType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_user1_item, parent, false);
            return new ViewHolder1(view, mItemClickListener);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_user3_item, parent, false);
            return new ViewHolder2(view, mItemClickListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        MessageDetails message = dataset.get(position);
        if (message.getMessagesentby() == ApplicationConstants.ADMIN_TYPE) {
            if (position == 0) {
                prevMsgIsAdmin = false;
            }
            if (position >= 1) {
                if (dataset.get(position - 1).getMessagesentby() == ApplicationConstants.ADMIN_TYPE) {
                    prevMsgIsAdmin = true;
                } else {
                    prevMsgIsAdmin = false;
                }
            }


            ((ViewHolder2) holder).messageTextView.setText(dataset.get(position).getMessage() + App.getApp().getResources().getString(R.string.space));
            ((ViewHolder2) holder).timeTextView.setText(UiUtils.getTimeStampString(dataset.get(position).getCreated()));

            if (dataset.get(position).getSeen() != null) {

                ((ViewHolder2) holder).messageStatus.setImageResource(R.drawable.doubletick);
                ((ViewHolder2) holder).messageStatus.setColorFilter(App.getApp().getResources().getColor(R.color.sky_blue));
            } else if (dataset.get(position).getDelivered() != null) {

                ((ViewHolder2) holder).messageStatus.setImageResource(R.drawable.doubletick);
                ((ViewHolder2) holder).messageStatus.setColorFilter(App.getApp().getResources().getColor(R.color.divider));
            } else {

                ((ViewHolder2) holder).messageStatus.setImageResource(R.drawable.singletick);
                ((ViewHolder2) holder).messageStatus.setColorFilter(App.getApp().getResources().getColor(R.color.divider));

            }

            setOutgoingMessageBg((ViewHolder2) holder);
        } else {

            if (position == 0) {
                prevMsgIsUser = false;
            }
            if (position >= 1) {
                if (dataset.get(position - 1).getMessagesentby() == ApplicationConstants.CLIENT_TYPE) {
                    prevMsgIsUser = true;
                } else {
                    prevMsgIsUser = false;
                }
            }

            ((ViewHolder1) holder).messageTextView.setText(dataset.get(position).getMessage() + App.getApp().getResources().getString(R.string.space));
            ((ViewHolder1) holder).timeTextView.setText(UiUtils.getTimeStampString(dataset.get(position).getCreated()));
            setIncomingMessageBg((ViewHolder1) holder);
        }

    }

    private void setOutgoingMessageBg(ViewHolder2 holder) {
        if (prevMsgIsAdmin) {
            setOutGoingBackground(holder, R.drawable.balloon_outgoing_normal_ext);
        } else {
            setOutGoingBackground(holder, R.drawable.balloon_outgoing_normal1);
        }
    }



    private void setIncomingMessageBg(ViewHolder1 holder) {
        if (prevMsgIsUser) {
            setIncomingBackground(holder, R.drawable.balloon_incoming_normal_ext);
        } else {
            setIncomingBackground(holder, R.drawable.balloon_incoming_normal1);
        }
    }

    private void setIncomingBackground(ViewHolder1 holder, int drawableId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.frameLayout.setBackground(App.getApp().getResources().getDrawable(drawableId));
        } else {
            holder.frameLayout.setBackgroundDrawable(App.getApp().getResources().getDrawable(drawableId));
        }
    }

    private void setOutGoingBackground(ViewHolder2 holder, int drawableId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            holder.chatLayout.setBackground(App.getApp().getResources().getDrawable(drawableId));
        } else {
            holder.chatLayout.setBackgroundDrawable(App.getApp().getResources().getDrawable(drawableId));
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, MessageDetails q);
    }

    private class ViewHolder1 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView messageTextView;
        public TextView timeTextView;
        public FrameLayout frameLayout;

        public ViewHolder1(View itemView, OnItemClickListener listener) {
            super(itemView);
            messageTextView = (TextView) itemView.findViewById(R.id.message_text);
            timeTextView = (TextView) itemView.findViewById(R.id.time_text);
            frameLayout = (FrameLayout) itemView.findViewById(R.id.frameholder);
            mItemClickListener = listener;
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
            }
        }

    }

    private class ViewHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView messageStatus;
        public TextView messageTextView;
        public TextView timeTextView;
        public ChatLayout chatLayout;

        public ViewHolder2(View itemView, OnItemClickListener listener) {
            super(itemView);

            messageTextView = (TextView) itemView.findViewById(R.id.message_text);
            timeTextView = (TextView) itemView.findViewById(R.id.time_text);
            chatLayout = (ChatLayout) itemView.findViewById(R.id.bubble);
            messageStatus = (ImageView) itemView.findViewById(R.id.user_reply_status);

            mItemClickListener = listener;
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {

            }
        }
    }

}
